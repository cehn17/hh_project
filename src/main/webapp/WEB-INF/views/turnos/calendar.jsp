<%--
  Created by IntelliJ IDEA.
  User: Gustav
  Date: 15/9/2019
  Time: 11:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title><spring:message code="calendar.turno.reservas" /></title>

	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
	<link href="<c:url value='/static/core/main.css' />" rel='stylesheet' >
	<link href="<c:url value='/static/daygrid/main.css' />" rel='stylesheet' >
	<link href="<c:url value='/static/timegrid/main.css' />" rel='stylesheet' >
	<link href="<c:url value='/static/list/main.css' />" rel='stylesheet' >

	<script type="text/javascript" src="<c:url value='/static/core/main.js' />"> </script>
	<script type="text/javascript" src="<c:url value='/static/core/locales/es.js' />"> </script>
	<script type="text/javascript" src="<c:url value='/static/interaction/main.js' />"></script>
	<script type="text/javascript" src="<c:url value='/static/daygrid/main.js' />"></script>
	<script type="text/javascript" src="<c:url value='/static/timegrid/main.js' />"></script>
	<script type="text/javascript" src="<c:url value='/static/list/main.js' />"></script>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	
	<link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">
	<style>
		#main-card{
			width: 80%;
		}
	</style>
</head>
<body class="text-center">

<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
	<div id="main-card" class="card text-center">
		<div class="card-header"><spring:message code="calendar.turno.reservas" /></div>
		<div class="card-body table-responsive"  style="text-align: left !important;">
			<div id='calendar'></div>
		</div>
	</div>
</div>
<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
<script>
	function getUrlTurnoList(){
		return 'http://'+window.location.host+'<c:url value="/turnos/list"/>';
	}
	function getUrlTurnoId(id){
		return 'http://'+window.location.host+'<c:url value="/turnos/verdetalle"/>'+'?turnoId='+id;
	}

	document.addEventListener('DOMContentLoaded', function() {
		var calendarEl = document.getElementById('calendar');

		var calendar = new FullCalendar.Calendar(calendarEl, {
			locale: '<spring:message code="buscar.idioma" />',
			plugins: [ 'dayGrid', 'timeGrid', 'list', 'interaction' ],
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
			},
			defaultDate: new Date(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: {
				url : getUrlTurnoList(),
				method : 'GET',
				failure: function(){
					alert('<spring:message code="calendar.turno.error" />');
				}
			},
			eventClick: function(info) {
				window.open(getUrlTurnoId(info.event.id));
			}
		});
		calendar.render();
	});
</script>
</html>