<%--
  Created by IntelliJ IDEA.
  User: Gustav
  Date: 8/9/2019
  Time: 14:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title><spring:message code="signin.title"/></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="<c:url value='/static/css/signin.css' />" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">
</head>
<body class="text-center bg-dark">
<c:url var="loginUrl" value="/user/login" />
<div class="container">
        <div class="row">
			<div class="col-md-5 mx-auto">
<form action="${loginUrl}" method="post" class="form-signin bg-white rounded">
    <img class="mb-4" src="<c:url value='/static/images/hh3.png' />" alt="" width="300" height="200">
    <h1 class="h3 mb-3 font-weight-normal"><spring:message code="signin.name.h1"/></h1>

    <label for="username" class="sr-only"><spring:message code="signin.username.label"/></label>
    <input id="username" name="ssoId" class="form-control" placeholder="<spring:message code="signin.username.placeholder"/>" required autofocus="autofocus">

    <label for="password" class="sr-only"><spring:message code="signin.password.label"/></label>
    <input name="password" type="password" id="password" class="form-control" placeholder="<spring:message code="signin.password.label"/>" required>
    <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
    <c:if test="${param.error != null}">
        <div class="alert alert-danger" role="alert">
            <p><spring:message code="signin.error"/></p>
        </div>
    </c:if>
    <c:if test="${param.logout != null}">
        <div class="alert alert-success" role="alert">
            <p><spring:message code="signin.logout"/></p>
        </div>
    </c:if>
    <c:if test="${param.refreshOk != null}">
        <div class="alert alert-success" role="alert">
            <p><spring:message code="signin.newpass"/></p>
        </div>
    </c:if>
    <a href="<c:url value="/user/refreshpassform" />"><spring:message code="signin.password.link"/></a>
    <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message code="signin.name.button"/></button>
    <p class="mt-5 mb-3 text-muted"><spring:message code="signin.copyright"/></p>
</form>
</div>
		</div>
      </div>   
</body>

</html>