<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
	<div class="authbar">
		<span><spring:message code="authheader.dear"/> <strong>${loggedinuser}</strong><spring:message code="authheader.welcome"/></span> <span class="floatRight"><a href="<c:url value="/logout" />"><spring:message code="authheader.logout"/></a></span>
	</div>
