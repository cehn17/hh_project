<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><spring:message code="form.service.registry" /></title>
    <spring:message code="form.service.placeholder.name" var="servicename"/>
    <spring:message code="form.service.placeholder.tpd" var="servicetimeprom"/>
    <spring:message code="form.service.placeholder.description" var="servicedescr"/>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card{
            width: 80%;
        }
        
        .error {
        color: #ff0000;
    }
    </style>
</head>
<body class="text-center">
<jsp:include page="../includes/menu.jsp"></jsp:include>
<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="form.service.registry" />
        </div>
        <spring:hasBindErrors name="serviceDTO">
			<div class='alert alert-danger' role='alert'>
				Por favor corrija los siguientes errores:
				<ul>
					<c:forEach var="error" items="${errors.allErrors}">
						<li><spring:message message="${error}" /></li>
					</c:forEach>
				</ul>
			</div>
		</spring:hasBindErrors>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <form:form method="POST" modelAttribute="serviceDTO">
                <form:input type="hidden" path="id" id="id"/>
                <div class="form-group row">
                    <label for="nombre" class="col-sm-2 col-form-label"><spring:message code="form.service.name" /></label>
                    <div class="col-sm-10">
<%--                        <input type="text" name="name" id="name" class="form-control" placeholder="Name" required="required"/>--%>
                        <form:input type="text" path="nombre" id="nombre" class="form-control" placeholder="${servicename}" required="required"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tiempoPromedioDeDuracion" class="col-sm-2 col-form-label"><spring:message code="form.service.tpd" /></label>
                    <div class="col-sm-10">
<%--                        <input type="time"class="form-control" id="avgTime" name="avgTime" placeholder="Avg Time" min="00:00" max="08:00" required>--%>
                        <form:input type="number" path="tiempoPromedioDeDuracion" id="avgTime" class="form-control" placeholder="${servicetimeprom}" required="required"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="precio" class="col-sm-2 col-form-label"><spring:message code="form.service.price" /></label>
                    <div class="col-sm-10">
<%--                        <input type="number" min="0" class="form-control" id="price" name="price" step="0.01" placeholder="0.00" required>--%>
                        <form:input type="number" path="precio" id="precio" class="form-control" placeholder="Price" required="required"/>
                    </div>
                </div>
					<c:choose>
						<c:when test="${edit}">
							<div class="form-group row">
								<label for="estado" class="col-sm-2 col-form-label"><spring:message code="form.service.state" /></label>
								<div class="col-sm-10">

									<form:select class="form-control" path="estado">
										<form:option value="ACTIVO"><spring:message code="form.service.state.active" /></form:option>
										<form:option value="INACTIVO"><spring:message code="form.service.state.inactive" /></form:option>
									</form:select>

								</div>
							</div>
						</c:when>
					</c:choose>
					<div class="form-group row">
                    <label for="descripcion" class="col-sm-2 col-form-label"><spring:message code="form.service.description" /></label>
                    <div class="col-sm-10">
<%--                        <textarea type="textarea" name="description" id="description" class="form-control" placeholder="Description" rows="3"></textarea>--%>
                        <form:textarea path="descripcion" class="form-control" placeholder="${servicedescr}" rows="3" required="required" />
                    </div>
                </div>
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="<spring:message code="form.service.button.update" />" class="btn btn-primary"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="<spring:message code="form.service.button.add" />" class="btn btn-primary"/>
                    </c:otherwise>
             	</c:choose>
                
            </form:form>
        </div>
    </div>
</div>
<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
<script>

    $(document).ready(function(){
        $('#precio').attr({
            "min":"0",
            "step":"0.01",
            "placeholder":"0.00"
        });
    });
</script>
</html>
