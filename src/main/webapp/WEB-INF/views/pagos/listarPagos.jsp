<%@ page contentType="text/html" language="java" 
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="pagolist.name" /></title>

<spring:url value="/pagos/create" var="urlCreate" />
<spring:url value="/pagos/delete" var="urlDelete" />
<spring:url value="/pagos/edit" var="urlEdit" />
<spring:url value="/pagos" var="urlPagos" />

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">


<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
	
	<link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

<style>
#main-card {
	width: 80%;
}
</style>
</head>

<body class="text-center">
<jsp:include page="../includes/menu.jsp"></jsp:include>
	<div class="d-flex justify-content-center py-4">
		<div id="main-card" class="card text-center">

			<div class="card-header">
				<spring:message code="pagolist.name" />
			</div>
			
			<div class="card-body table-responsive"
				style="text-align: left !important;">
				<table id="table-<spring:message code="buscar.idioma" />"
					class="table table-striped table-bordered dt-responsive nowrap"
					style="width: 100%">

					<thead>
						<tr>
							<th><spring:message code="pagolist.table.header.id" /></th>
							<th><spring:message code="pagolist.table.header.price" /></th>
							<th><spring:message code="pagolist.table.header.state" /></th>
							<th><spring:message code="pagolist.table.header.fecha" /></th>
							<!-- <th width="100"></th> -->
							<!-- <th width="100"></th> -->

						</tr>
					</thead>
					<tbody>
						<c:forEach items="${pagos}" var="pago">
							<tr>
								<td>${pago.id}</td>
								<td>${pago.montoTotal}</td>
								<td>${pago.estado}</td>
								<td><fmt:formatDate value="${pago.fecha}" pattern="dd-MM-yyyy"/></td>
								<!-- <td><a href="#" class="btn btn-danger custom-width">delete</a></td> -->
								<%--								<td><a href="<c:url value='/servicios/delete/${servicio.id}'/>" class="btn btn-danger custom-width">delete</a></td>--%>
								<%-- <td><a href="<c:url value='/pagos/imprimir/${pago.id}'/>"
									class="btn btn-success custom-width">Comprobante</a></td> --%>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="well">
					<a href="${urlCreate}" class="btn btn-primary"><spring:message code="pagolist.table.button.add" /></a>
				</div>
			</div>
		</div>


	</div>

	<script>
		$(document)
				.ready(
						function() {
							$('#table-es')
									.DataTable(
											{
												"language" : {
													"sProcessing" : "Procesando...",
													"sLengthMenu" : "Mostrar _MENU_ registros",
													"sZeroRecords" : "No se encontraron resultados",
													"sEmptyTable" : "Ning\u00fan dato disponible en esta tabla",
													"sInfo" : "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
													"sInfoEmpty" : "Mostrando registros del 0 al 0 de un total de 0 registros",
													"sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
													"sInfoPostFix" : "",
													"sSearch" : "Buscar:",
													"sUrl" : "",
													"sInfoThousands" : ",",
													"sLoadingRecords" : "Cargando...",
													"oPaginate" : {
														"sFirst" : "Primero",
														"sLast" : "\u00daltimo",
														"sNext" : "Siguiente",
														"sPrevious" : "Anterior"
													},
													"oAria" : {
														"sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
														"sSortDescending" : ": Activar para ordenar la columna de manera descendente"
													}
												}

											});

							$('#table-en').DataTable();
						});
	</script>
	<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
</html>
