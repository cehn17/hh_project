<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><spring:message code="detail.promocion" /></title>
    
    <spring:url value="/promociones/edit" var="urlEdit" />
    <spring:url value="/promociones/list" var="urlList" />
    <spring:url value="/servicios/detail" var="urlDetailServicio" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>

<body class="text-center">
<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="detail.promocion" />
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <form:form method="POST" modelAttribute="promocionDTO"  >
                <dl class="row">
                	<dt class="col-sm-3"><spring:message code="detail.promocion.name" /></dt>
  					<dd class="col-sm-9">${promocionDTO.nombre}</dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.promocion.discount" /></dt>
  					<dd class="col-sm-9">${promocionDTO.descuento}</dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.promocion.fechaAlta" /></dt>
  					<dd class="col-sm-9">${promocionDTO.fechaDeAlta}</dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.promocion.fechaVencimiento" /></dt>
  					<dd class="col-sm-9">${promocionDTO.fechaDeVencimiento}</dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.promocion.fechaBaja" /></dt>
  					<dd class="col-sm-9">${promocionDTO.fechaDeBaja}</dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.promocion.state" /></dt>
  					<c:choose>
						<c:when test="${promocionDTO.estado eq 'ACTIVO'}">
  							<dd class="col-sm-9"><spring:message code="detail.promocion.state.active" /></dd>
  						</c:when>
						<c:otherwise>
							<dd class="col-sm-9"><spring:message code="detail.promocion.state.inactive" /></dd>
						</c:otherwise>
					</c:choose>
  					
  					<dt class="col-sm-3"><spring:message code="detail.promocion.valorFinal" /></dt>
  					<dd class="col-sm-9">${promocionDTO.valorFinal}</dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.promocion.services" /></dt>
                </dl>
                
                <!-- <div class="card-header">Servicios:</div> -->
                
                <!-- <div class="card-body table-responsive"
				style="text-align: left !important;"> -->
				<table id="users-table-es"
					class="table table-striped table-bordered dt-responsive nowrap"
					style="width: 100%">

					<thead>
						<tr>
							<th><spring:message code="servicelist.table.header.name" /></th>
							<th><spring:message code="servicelist.table.header.price" /></th>
							<th><spring:message code="servicelist.table.header.timeprom" /></th>
							<th></th>
							<%-- <th><spring:message code="servicelist.table.header.state" /></th>
							<th><spring:message code="servicelist.table.header.fechaBaja" /></th> --%>
							<%-- <th><spring:message code="servicelist.table.header.description" /></th> --%>
							

						</tr>
					</thead>
					<tbody>
						<c:forEach items="${serviciosDTO}" var="servicio">
							<tr>
								<td>${servicio.nombre}</td>
								<td>$ ${servicio.precio}</td>
								<td>${servicio.tiempoPromedioDeDuracion} <spring:message code="detail.service.minutes" /></td>
								<%-- <td>${servicio.estado}</td>
								<td>${servicio.fechaDeBaja}</td>
 --%>								<%-- <td>${servicio.descripcion}</td> --%>
 								<td><a href="${urlDetailServicio}/${servicio.id}" class="btn btn-info"><spring:message code="servicelist.table.button.detail" /></a></td>
								
						</c:forEach>
					</tbody>
				</table>
			<!-- </div> -->
			
			<a href="${urlEdit}/${promocionDTO.id}" class="btn btn-success"><spring:message code="detail.promocion.button.edit" /></a>
			<a href="${urlList}" class="btn btn-primary"><spring:message code="detail.promocion.button.return" /></a>
            </form:form>
        </div>
    </div>
</div>
<jsp:include page="../includes/footer.jsp"></jsp:include>

	<script>
    $('#fechaDeVencimiento').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'dd-mm-yyyy',
        startDate: '-3d'
    });
    
    new SlimSelect({
    	  select: '#multiple'
    })
    
	console.log("FC");
	</script>
</body>
</html>