<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><spring:message code="list.promocion" /></title>
    
    <spring:url value="/promociones/create" var="urlCreate" />
	<spring:url value="/promociones/delete" var="urlDelete" />
	<spring:url value="/promociones/edit" var="urlEdit" />
	<spring:url value="/promociones/active" var="urlActive" />
	<spring:url value="/promociones/detail" var="urlDetail" />
	<spring:url value="/promociones" var="urlPromociones" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
    
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>

    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>
<body class="text-center">

<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header"><spring:message code="list.promocion" /></div>
        
        <c:if test="${mensaje!=null }">
      	
      	<div class='alert alert-success' role="alert">${mensaje}</div>
      	
      </c:if>
      
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <table id="table-<spring:message code="buscar.idioma" />" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th><spring:message code="list.promocion.name" /></th>
                    <th><spring:message code="list.promocion.discount" /></th>
                    <!-- <th>Fecha de Alta</th> -->
                    <th><spring:message code="list.promocion.fechaVencimiento" /></th>
                    <!-- <th>Fecha de Baja</th> -->
                    <th><spring:message code="list.promocion.state" /></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${promocionesDTO}" var="promocion">
            
                    <tr>
                        <td>${promocion.nombre}</td>
                        <td>${promocion.descuento}</td>
                        <%-- <td><fmt:formatDate pattern="dd-MM-yyyy" value="${promocion.fechaDeAlta}" /></td> --%>
                        <td>${promocion.fechaDeVencimiento}</td>
                        <%-- <td><fmt:formatDate pattern="dd-MM-yyyy" value="${promocion.fechaDeBaja}" /></td> --%>
                        <c:choose>
							<c:when test="${promocion.estado eq 'ACTIVO'}">
								<td><spring:message code="list.promocion.state.active" /></td>
							</c:when>
							<c:otherwise>
								<td><spring:message code="list.promocion.state.inactive" /></td>
							</c:otherwise>
						</c:choose>
                        
                        <td><a href="${urlDetail}/${promocion.id}" class="btn btn-info"><spring:message code="list.promocion.button.detail" /></a></td>
                        <td><a href="${urlEdit}/${promocion.id}" class="btn btn-success custom-width"><spring:message code="list.promocion.button.edit" /></a></td>
								<c:choose>
						<c:when test="${promocion.fechaDeBaja eq null}">
							<td><a href="${urlDelete}/${promocion.id}" onclick='return confirm("Estas seguro?")' class="btn btn-danger custom-width"><spring:message code="list.promocion.button.delete" /></a></td>
						</c:when>
						<c:otherwise>
							<td><a href="${urlActive}/${promocion.id}" class="btn btn-primary custom-width"><spring:message code="list.promocion.button.active" /></a></td>
						</c:otherwise>
					</c:choose>
								
							
                    </tr>
                </c:forEach>
                </tbody>
            </table>
               <div class="well">
					<a href="${urlCreate}" class="btn btn-primary"><spring:message code="list.promocion.button.new" /></a>
				</div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table-es').DataTable({
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ning\u00fan dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "\u00daltimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }

        });

        $('#table-en').DataTable();
    } );
</script>

<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
</html>