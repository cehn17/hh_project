<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><spring:message code="form.profesional" /></title>
    <spring:message code="form.profesional.name" var="profesionalName" />
	<spring:message code="form.profesional.lastname" var="profesionalLastName" />
	<spring:message code="form.profesional.phone" var="profesionalPhone" />
	<spring:message code="form.profesional.fechaNacimiento" var="profesionalFechaNacimiento" />
	<spring:message code="form.profesional.fechaBaja" var="profesionalFechaBaja" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.css" rel="stylesheet"/>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <!-- Datatable responsive -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="<c:url value='/static/datepicker/jquery-ui.js' />"></script>
    <script src="<c:url value='/static/datepicker/datepicker-es.js' />"></script>
    
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>
<body class="text-center">

<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="form.profesional" />
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
        	<c:url var="addService" value="/profesionales/nuevoservicio" />
            <form:form id="formProfesionales" method="POST" modelAttribute="profesionalDTO" action="${addService}">
                
                <form:input type="hidden" value="${profesionalDTO.id}" path="id" id="id"/>
                <div class="form-group row">
                    <label for="nombre" class="col-sm-2 col-form-label"><spring:message code="form.profesional.name" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="nombre" id="nombre" class="form-control" placeholder="${profesionalName}" value="${profesional.nombre}" required="required"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="apellido" class="col-sm-2 col-form-label"><spring:message code="form.profesional.lastname" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="apellido" id="apellido" class="form-control" placeholder="${profesionalLastName}" value="${profesional.apellido}" required="required"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label"><spring:message code="form.profesional.email" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="email" id="email" class="form-control" placeholder="Email" value="${profesional.email}" required="required"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="telefono" class="col-sm-2 col-form-label"><spring:message code="form.profesional.phone" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="telefono" id="telefono" class="form-control" placeholder="${profesionalPhone}" value="${profesional.telefono}"/>
                    </div>
                </div>
                
                <div class="form-group row">
						<label for="estado" class="col-sm-2 col-form-label"><spring:message code="profesionaleslist.table.header.status" /></label>
						<div class="col-sm-10">
							<form:select class="form-control" path="estado" readonly="true">
								<form:option value="ACTIVO"><spring:message code="profesionaleslist.table.header.status.active" /></form:option>
								<form:option value="INACTIVO"><spring:message code="profesionaleslist.table.header.status.inactive" /></form:option>
								<form:option value="LICENCIA"><spring:message code="profesionaleslist.table.header.status.license" /></form:option>
								<form:option value="VACACIONES"><spring:message code="profesionaleslist.table.header.status.holidays" /></form:option>
							</form:select>
						</div>
				</div>

				<div class="form-group row">
					<label for="fechaDeNacimiento" class="col-sm-2 col-form-label"><spring:message code="form.profesional.fechaNacimiento" /></label>

					<div class="col-sm-10">
						<form:input path="fechaDeNacimiento" id="fechaDeNacimiento"
							class="form-control" value="${profesional.fechaDeNacimiento}" placeholder="${profesionalFechaNacimiento}" />
					</div>
				</div>
				
			<c:url var="urlList" value="/profesionales/edit" />
			 <c:choose>
                  <c:when test="${edit}">
                      <input type="submit" formmethod="post" formaction="${urlList}" value="<spring:message code="form.profesional.button.edit" />" class="btn btn-success"/>
                  </c:when>
                  <c:otherwise>
					<input type="submit" formmethod="post" formaction="${urlList}" value="<spring:message code="form.profesional.button.create" />" class="btn btn-primary"/>
                  </c:otherwise>
              </c:choose>

                <div class="d-flex justify-content-center py-4" style="width: 100%">
                    <div class="card text-center" style="width: 100%">
                        <div class="card-header">
                            <spring:message code="form.profesional.services" />
                        </div>
                        <div class="card-body table-responsive"  style="text-align: left !important;">
                            <table id="servicio-prof" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th class="col-sm-10"><spring:message code="form.profesional.services.name" /></th>
                                    <th class="col-sm-10"></th>
                                </tr>
                                </thead>
                                <tbody>

                                <c:forEach items="${profesionalesServiciosList}" var="servicio">
                                    <tr>
                                        <td class="col-sm-10">${servicio.nombre}</td>
                                        <c:url var="profServ" value="/profesionales/deleteServicio/${servicio.id}" />
										<td class="col-sm-10"><input type="submit" value="<spring:message code="form.profesional.services.remove" />" formmethod="post" formaction="${profServ}" 
										onclick='return confirm("<spring:message code="form.profesional.services.seguro" />")' class="btn btn-danger custom-width"/>
										</td>
                                    </tr>
                                </c:forEach>

                                </tbody>
                            </table>


                            <label class="my-1 mr-2" for="inlineFormCustomSelectPref"><spring:message code="form.profesional.services.service" /></label>
                            <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="servicio">
                                <option selected><spring:message code="form.profesional.services.selection" /></option>
                                <c:forEach items="${serviciosList}" var="servicio">
                                    <option value="${servicio.id}">${servicio.nombre}</option>
                                </c:forEach>
                            </select>

                           <input id="agregarServicio" type="button" value="<spring:message code="form.prefesional.services.add" />" class="btn btn-primary"/> 
                      
                        </div>
                    </div>
                </div>
                <div>
                
                <h4 style="text-align:center;"><spring:message code="form.profesional.horario" /></h4>
                </div>
                  <div>
                  
                         <c:forEach items="${listaDisponibles}" var="disponibles">
                         <table id="disponibilidadProf" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%; text-align:center">
                             <thead class="table-info">
                             
                             <c:choose>
								<c:when test="${disponibles.dia eq 'LUNES'}">
								<tr>
									<th class="col-sm-10" colspan="3"><spring:message code="form.profesional.disponibilidad.lunes" /></th>
								 </tr>
								</c:when>
								<c:when test="${disponibles.dia eq 'MARTES'}">
								<tr>
									<th class="col-sm-10" colspan="3"><spring:message code="form.profesional.disponibilidad.martes" /></th>
								 </tr>
								</c:when>
								<c:when test="${disponibles.dia eq 'MIERCOLES'}">
								<tr>
									<th class="col-sm-10" colspan="3"><spring:message code="form.profesional.disponibilidad.miercoles" /></th>
								 </tr>
								</c:when>
								<c:when test="${disponibles.dia eq 'JUEVES'}">
								<tr>
									<th class="col-sm-10" colspan="3"><spring:message code="form.profesional.disponibilidad.jueves" /></th>
								 </tr>
								</c:when>
								<c:when test="${disponibles.dia eq 'VIERNES'}">
								<tr>
									<th class="col-sm-10" colspan="3"><spring:message code="form.profesional.disponibilidad.viernes" /></th>
								 </tr>
								</c:when>
								<c:when test="${disponibles.dia eq 'SABADO'}">
								<tr>
									<th class="col-sm-10" colspan="3"><spring:message code="form.profesional.disponibilidad.sabado" /></th>
								 </tr>
								</c:when>
								
								
								<c:otherwise>
									<tr>
                                 		<th class="col-sm-10" colspan="3"><spring:message code="form.profesional.disponibilidad.domingo" /></th>
                                	</tr>
								</c:otherwise>
							</c:choose>
                             
                             	
                                <c:choose>
							       <c:when test="${not empty disponibles.horaDisponible}">
						               <th class="col-sm-30"><spring:message code="form.profesional.horario.inicio" /></th>
		                               <th class="col-sm-30"><spring:message code="form.profesional.horario.fin" /></th>
		                               <th class="col-sm-30"></th>
							       </c:when>
							       <c:otherwise>

							       </c:otherwise>
							     </c:choose>
                                
                            
                             </thead>
                             <tbody>
                                
								<c:forEach items="${disponibles.horaDisponible}" var="horario">
								<tr>
						          <td class="col-sm-30"><input type="text" value="${horario.horaInicio}" readonly/>
									</td>
									<td class="col-sm-30"><input type="text" value="${horario.horaFin}" readonly/>
									</td>
									<c:url var="profDisp" value="/profesionales/deleteDisponibilidad/${disponibles.id}/${horario.id}" />
									
									 <td class="col-sm-30"><input type="submit" value="<spring:message code="form.profesional.horario.remove" />" formmethod="post" formaction="${profDisp}"
										onclick='return confirm("<spring:message code="form.profesional.horario.seguro" />")' class="btn btn-danger custom-width"/>
									</td>
									
								</tr>
								
						         </c:forEach>
						         <tr>			
									<td class="col-sm-10" colspan="4">
									    <div class="well" align="center">
												<button type="button" class="btn btn-primary" data-toggle="modal"
												data-target="#modalDisponibilidad" data-idProfesional="${profesionalDTO.id}" data-dia="${disponibles.dia}"><spring:message code="form.profesional.horario.addDisponibilidad" /></button>
										</div>
									</td>
								</tr>

						
                             </tbody>
                         </table>
                    </c:forEach>
                  </div>
                  
                  <%--modal--%>
   
		
			<div id="modalDisponibilidad" class="modal fade bd-example-modal-lg" tabindex="-1"
				role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel"><spring:message code="form.profesional.horario.newDisponibilidad" /></h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						
						<div class="modal-body">
								<input type="hidden" id="idProfesional" name="idProfesional">
								<input type="hidden" id="dia" name="dia">

								<label class="my-1 mr-2" for="horaInicio"><spring:message code="form.profesional.horario.inicio" /></label>
								<input class="my-1 mr-2" id="horaInicio" type="time" name="horaInicio"/>

								<label class="my-1 mr-2" for="horaFin"><spring:message code="form.profesional.horario.fin" /></label>
								<input class="my-1 mr-2" id="horaFin" type="time" name="horaFin"/>
						<c:url var="nuevaDisponibilidad" value="/profesionales/nuevaDisponibilidad" />	
				  		<input type="submit" id="botonDisp" formmethod="post" formaction="${nuevaDisponibilidad}" value="<spring:message code="form.prefesional.services.add" />" class="btn btn-primary"/>
						</div>
					</div>
				</div>
			</div>
		
		
		   <%--modal--%>
                  
            </form:form>
        </div>
   
   
   
    </div>
</div>

<jsp:include page="../includes/footer.jsp"></jsp:include>

</body>
<script>

    $(document).ready(function(){
        //$('#servicios').DataTable();

        $('#precio').attr({
            "min":"0",
            "step":"0.01",
            "placeholder":"0.00"
        });

        $('#avgTime').attr({
            "min":"00:00",
            "max":"07:59",
        });
        $('#fechaDeNacimiento').datepicker();
        $.datepicker.setDefaults( $.datepicker.regional[ "<spring:message code="buscar.idioma" />" ] );

        $('#timepicker').timepicker({
            uiLibrary: 'bootstrap4'
        });
        
    });
    
    $('#modalDisponibilidad').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) 
        var recipient = button.data('whatever');
        var idProfesional = button.attr('data-idProfesional');
        var dia = button.attr('data-dia');
        console.log('dia ' + dia);
        console.log('idProf ' + idProfesional);
        var modal = $(this)
        $('#idProfesional').val(idProfesional);
        $('#dia').val(dia);
        $('#horaInicio').prop('required',true);
        $('#horaFin').prop('required',true);
    })
    
    $('#modalDisponibilidad').on('hidden.bs.modal', function (e) {
    	$('#horaInicio').prop('required',false);
    	$('#horaFin').prop('required',false);
	})

	$('#horaInicio').on('keyup click', function(){
        console.log($('#horaInicio').val());
		$("#horaFin").attr({
	        "min": $('#horaInicio').val()
	      });
	});
    
    $("#agregarServicio").on("click", function(e){
    	e.preventDefault();
    	var servicios = [];
    	var servicioSeleccionado;
    	$('#servicio-prof tr').each(function() {
    	   var customerId = $(this).find("td").html(); 
    	   if(typeof customerId != "undefined" && customerId != '' && customerId != null){
    		   servicios.push(customerId);
    	   }    	   
    	 });
    	//var values = $("#inlineFormCustomSelectPref option").map(function() { return $(this).text(); }).get();
    	//todosServicios = values;

    	servicioSeleccionado = $('#inlineFormCustomSelectPref option:selected').html();
    	if(jQuery.inArray( servicioSeleccionado, servicios) < 0 && servicioSeleccionado != "<spring:message code="form.profesional.services.selection" />"){
    		 $('#formProfesionales').submit();
    		 return;
    	}
    	alert("<spring:message code="form.profesional.horario.serviceCargado" />");
    	return;
	});
    
</script>
</html>

