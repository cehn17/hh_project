<%@ page contentType="text/html" language="java" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><spring:message code="profesionaleslist.name" /></title>

    <spring:url value="/profesionales/create" var="urlCreate" />
    <spring:url value="/profesionales/delete" var="urlDelete" />
    <spring:url value="/profesionales/edit" var="urlEdit" />
    <spring:url value="/profesionales" var="urlProfesionales" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card {
            width: 80%;
        }
    </style>
</head>

<body class="text-center">

<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">

        <div class="card-header">
            <spring:message code="profesionaleslist.name" />
        </div>
        <div class="card-body table-responsive"
             style="text-align: left !important;">
            <table id="profesionales-table-<spring:message code="buscar.idioma" />" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%">
                <thead>
                <tr>
                    <th><spring:message code="profesionaleslist.table.header.name" /></th>
                    <th><spring:message code="profesionaleslist.table.header.surname" /></th>
                    <th><spring:message code="profesionaleslist.table.header.email" /></th>
                    <th><spring:message code="profesionaleslist.table.header.telephone" /></th>
                    <th><spring:message code="profesionaleslist.table.header.status" /></th>
                    <th><spring:message code="profesionaleslist.table.header.fechaBaja" /></th>
                    <th></th>
                    <th></th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${profesionales}" var="profesional">
                    <tr>
                        <td>${profesional.nombre}</td>
                        <td>${profesional.apellido}</td>
                        <td>${profesional.email}</td>
                        <td>${profesional.telefono}</td>
                        <c:choose>
							<c:when test="${profesional.estadoProfesional eq 'ACTIVO'}">
								<td><spring:message code="profesionaleslist.table.header.status.active" /></td>
							</c:when>
							<c:when test="${profesional.estadoProfesional eq 'LICENCIA'}">
								<td><spring:message code="profesionaleslist.table.header.status.license" /></td>
							</c:when>
							<c:when test="${profesional.estadoProfesional eq 'VACACIONES'}">
								<td><spring:message code="profesionaleslist.table.header.status.holidays" /></td>
							</c:when>
							<c:otherwise>
								<td><spring:message code="profesionaleslist.table.header.status.inactive" /></td>
							</c:otherwise>
						</c:choose>
                        <td>${profesional.fechaBaja}</td>
                        <td><a href="<c:url value='/profesionales/edit/${profesional.id}'/>" class="btn btn-success custom-width"><spring:message code="profesionaleslist.table.button.edit" /></a></td>
                        <c:choose>
                            <c:when test="${empty profesional.fechaBaja || profesional.fechaBaja eq null}">
                                <td><a href="<c:url value='/profesionales/delete/${profesional.id}'/>" onclick='return confirm("Estas seguro?")' class="btn btn-danger custom-width"><spring:message code="profesionaleslist.table.button.delete" /></a></td>
                            </c:when>
                            <c:otherwise>
                                <td><a href="<c:url value='/profesionales/active/${profesional.id}'/>" class="btn btn-primary custom-width"><spring:message code="profesionaleslist.table.button.active" /></a></td>
                            </c:otherwise>
                        </c:choose>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="well">
                <a href="${urlCreate}" class="btn btn-primary"><spring:message code="profesionaleslist.table.button.add" /></a>
            </div>
        </div>
    </div>


</div>

<script>
    $(document).ready(function() {
        $('#profesionales-table-es').DataTable({
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ning\u00fan dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "\u00daltimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }

        });

        $('#profesionales-table-en').DataTable();
    } );
</script>

<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
</html>