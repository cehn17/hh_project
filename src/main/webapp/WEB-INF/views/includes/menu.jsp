<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:url value="/" var="urlRoot"></spring:url>
<spring:url value="/locale" var="urlLocale"></spring:url>
<!-- Fixed navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="main_navbar">
  <a class="navbar-brand " href="${urlRoot}"><spring:message code="navbar.titulo" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
   		
   		<div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            
            	<li class="nav-item dropdown">
            		<sec:authorize access="hasRole('ADMIN') or hasRole('ADMINISTRATIVO')">
                    <a class=" nav-link btn btn-outline-primary dropdown-toggle" href="${urlRoot}turnos/listarturnos" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <spring:message code="navbar.administracion" />
                    </a>
                    </sec:authorize>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <sec:authorize access="hasRole('ADMIN') or hasRole('ADMINISTRATIVO') ">
                        <li><a class="dropdown-item" href="${urlRoot}turnos/listarturnos"><spring:message code="navbar.turnos" /></a></li>
                        <li><a class="dropdown-item" href="${urlRoot}servicios/list"><spring:message code="navbar.servicios" /></a></li>
                        <li><a class="dropdown-item" href="${urlRoot}profesionales/list"><spring:message code="navbar.profesionales" /></a></li>
                        <li><a class="dropdown-item" href="${urlRoot}clientes/list"><spring:message code="navbar.clientes" /></a></li>
                        <li><a class="dropdown-item" href="${urlRoot}turnos/calendar"><spring:message code="navbar.calendario" /></a></li>
                        <li><a class="dropdown-item" href="${urlRoot}promociones/list"><spring:message code="navbar.promociones" /></a></li>
                        <li><a class="dropdown-item" href="${urlRoot}sucursales/list"><spring:message code="navbar.sucursales" /></a></li>
                        <%-- <li><a class="dropdown-item" href="${urlRoot}pagos/list"><spring:message code="navbar.pagos" /></a></li> --%>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ADMIN')">
                    	<li><a class="dropdown-item" href="${urlRoot}user/list"><spring:message code="navbar.usuarios" /></a></li>
                	</sec:authorize>
                    </ul>
                </li>
                <%-- <sec:authorize access="hasRole('CONTADOR')">
                    <li class="nav-item active mx-1">
                    	<a class="nav-link btn btn-outline-primary" href="${urlRoot}pagos/list"><spring:message code="navbar.pagos" /></a>
                    </li>
                    </sec:authorize> --%>
                    <sec:authorize access="hasRole('ADMIN') or hasRole('CONTADOR')">
                    <li class="nav-item active mx-1"><a class="nav-link btn btn-outline-primary" href="${urlRoot}reportes/list"><spring:message code="navbar.reportes" /></a></li>
                    </sec:authorize>
                
                <sec:authorize access="hasRole('ADMIN')">
          		<li class="nav-item active mx-1">
            		<a class="nav-link btn btn-outline-primary" href="${urlRoot}config/list"><spring:message code="navbar.configuraciones" /></a>
          		</li>
        		</sec:authorize>
                
             </ul>
      
      		<ul class="navbar-nav navbar-right mx-4">
      			<li class="nav-item mx-1"><a class="btn btn-outline-danger" href="${urlRoot}user/logout"><spring:message code="navbar.salir" /></a></li>
				<li class="nav-item mx-1"><a class="btn btn-outline-info" href="${urlLocale}?lang=es_ES">ES</a></li>
				<li class="nav-item mx-1"><a class="btn btn-outline-info" href="${urlLocale}?lang=en_US">EN</a></li>
			</ul>
   </div>
    
  </nav>
  