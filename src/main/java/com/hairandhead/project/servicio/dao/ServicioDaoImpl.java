package com.hairandhead.project.servicio.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.hairandhead.project.servicio.model.EstadoServicio;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.utils.AbstractDao;

@Repository("servicioDao")
public class ServicioDaoImpl extends AbstractDao<Integer,Servicio> implements ServicioDao {

	@Override
	public Servicio findById(int id) {
		return super.getByKey(id);
	}

	@Override
	public void save(Servicio servicio) {
		super.persist(servicio);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Servicio> findAll() {
		Criteria criteria = createEntityCriteria();
		return (List<Servicio>) criteria.list();
	}

	@Override
	public void update(Servicio s) {
//		Criteria crit = createEntityCriteria();
//		crit.add(Restrictions.eq("id", id));
//		Servicio s = (Servicio)crit.uniqueResult();
		super.update(s);
	}

	@Override
	public List<Servicio> findAllServiciosActivos() {
		List<Servicio> ret = new ArrayList<Servicio>();
		for(Servicio s : findAll()) {
			if(s.getEstado().equals(EstadoServicio.ACTIVO))
				ret.add(s);
		}
 		return ret;
	}

}
