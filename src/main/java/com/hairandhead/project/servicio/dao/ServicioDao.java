package com.hairandhead.project.servicio.dao;

import java.util.List;

import com.hairandhead.project.servicio.model.Servicio;

public interface ServicioDao {

	Servicio findById(int id);
	
	public void save(Servicio servicio);
	
	public List<Servicio> findAll();
	
	public List<Servicio> findAllServiciosActivos();
	
	public void update(Servicio servicio);
	
}
