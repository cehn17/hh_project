package com.hairandhead.project.servicio.model;


import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

@Entity
@Table(name="Servicios")
public class Servicio{
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="Nombre", nullable=false)
	private String nombre;
	

	@Column(name="Descripcion", nullable=false)
	private String descripcion;
	

    @Digits(integer=8, fraction=2)
	@Column(name = "Precio")
	private BigDecimal precio;

	@Column(name = "TiempoPromedioDeDuracion")
	private Duration tiempoPromedioDeDuracion;

	@Column(name="Estado")
	private EstadoServicio estado;
	
	@Column(name="FechaDeBaja")
	private LocalDateTime fechaDeBaja;

	public Servicio(){
		this.id = null;
		this.nombre = "";
		this.descripcion = "";
		this.precio = new BigDecimal("0");
		this.tiempoPromedioDeDuracion = null;
		this.estado = EstadoServicio.ACTIVO;
		this.fechaDeBaja = null;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Duration getTiempoPromedioDeDuracion() {
		return tiempoPromedioDeDuracion;
	}

	public void setTiempoPromedioDeDuracion(Duration tiempoPromedioDeDuracion) {
		this.tiempoPromedioDeDuracion = tiempoPromedioDeDuracion;
	}

	public EstadoServicio getEstado() {
		return estado;
	}

	public void setEstado(EstadoServicio estado) {
		this.estado = estado;
	}

	public LocalDateTime getFechaDeBaja() {
		return fechaDeBaja;
	}

	public void setFechaDeBaja(LocalDateTime fechaDeBaja) {
		this.fechaDeBaja = fechaDeBaja;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servicio other = (Servicio) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (estado != other.estado)
			return false;
		if (fechaDeBaja == null) {
			if (other.fechaDeBaja != null)
				return false;
		} else if (!fechaDeBaja.equals(other.fechaDeBaja))
			return false;
		if (id != other.id)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (precio == null) {
			if (other.precio != null)
				return false;
		} else if (!precio.equals(other.precio))
			return false;
		if (tiempoPromedioDeDuracion == null) {
			if (other.tiempoPromedioDeDuracion != null)
				return false;
		} else if (!tiempoPromedioDeDuracion.equals(other.tiempoPromedioDeDuracion))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Servicio [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", precio=" + precio
				+ ", tiempoPromedioDeDuracion=" + tiempoPromedioDeDuracion + ", estado=" + estado + ", fechaDeBaja="
				+ fechaDeBaja + "]";
	}
}
