package com.hairandhead.project.servicio.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalUnit;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.EstadoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hairandhead.project.servicio.dao.ServicioDao;
import com.hairandhead.project.servicio.model.Servicio;

@Service("ServicioService")
@Transactional
public class ServicioServiceImpl implements ServicioService{

	@Autowired
	private ServicioDao dao;
	
	@Override
	public Servicio findServicioById(int id) {
		return dao.findById(id);
	}

	@Override
	public void saveServicio(Servicio servicio) {
		dao.save(servicio);
	}

	@Override
	public void updateServicio(Servicio servicio) {
//		Servicio entity = dao.findById(servicio.getId());
//		if(entity != null)
//		{
//			entity.setNombre(servicio.getNombre());
//			entity.setDescripcion(servicio.getDescripcion());
//			entity.setPrecio(servicio.getPrecio());
//			entity.setTiempoPromedioDeDuracion(servicio.getTiempoPromedioDeDuracion());
//			entity.setEstado(servicio.getEstado());
//			entity.setFechaDeBaja(servicio.getFechaDeBaja());
//		}
		dao.update(servicio);
	}

	@Override
	public List<Servicio> findAllServicios() {
		return dao.findAll();
	}

	@Override
	public void convertToDTO(Servicio source, ServicioDTO target) {
		System.out.println("Source: " + source);
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setDescripcion(source.getDescripcion());
//		target.setPrecio(source.getPrecio()!=null?currencyFormatter.format(source.getPrecio()):"");
		target.setPrecio(source.getPrecio()!=null?df.format(source.getPrecio()):"");
		target.setTiempoPromedioDeDuracion(source.getTiempoPromedioDeDuracion()!=null?
				//new SimpleDateFormat("HH:mm").format(source.getTiempoPromedioDeDuracion()):"");
				Long.valueOf(source.getTiempoPromedioDeDuracion().toMinutes()).toString(): "");
		target.setEstado(source.getEstado().name());
		target.setFechaDeBaja(source.getFechaDeBaja()!=null?source.getFechaDeBaja().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")).toString():null);
	}

	@Override
	public ServicioDTO convertToDTO(Servicio source) {
		ServicioDTO res = new ServicioDTO();
		convertToDTO(source, res);
		return res;
	}

	@Override
	public void convertToServicio(ServicioDTO source, Servicio target) {
		target.setId(source.getId());
		target.setNombre(source.getNombre());
		target.setDescripcion(source.getDescripcion());
		target.setPrecio(new BigDecimal(source.getPrecio()));

		try {
			//Duration dur1 = new Duration(Long.valueOf(source.getTiempoPromedioDeDuracion());
			Duration dur = Duration.ofMinutes(Long.valueOf(source.getTiempoPromedioDeDuracion()));
			target.setTiempoPromedioDeDuracion((source.getTiempoPromedioDeDuracion()!=null&&!source.getTiempoPromedioDeDuracion().isEmpty())?
					dur:null);
//			target.setFechaDeBaja((source.getFechaDeBaja()!=null&&!source.getFechaDeBaja().isEmpty())?
//					new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(source.getFechaDeBaja()):null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		target.setFechaDeBaja("INACTIVO".equals(source.getEstado())?LocalDateTime.now():null);
		target.setEstado("INACTVIO".equals(source.getEstado()) || "INACTIVO".equals(target.getEstado().name())?EstadoServicio.INACTIVO:EstadoServicio.ACTIVO);
	}

	@Override
	public List<Servicio> findAllServiciosActivos() {
		// TODO Auto-generated method stub
		return dao.findAllServiciosActivos();
	}

	
}
