package com.hairandhead.project.servicio.dto;

import java.util.Objects;

public class ServicioJsonDTO {
	 private Integer value;

	    private String text;

	    public ServicioJsonDTO() {
	        this.value = null;
	        this.text = "";
	    }

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ServicioJsonDTO that = (ServicioJsonDTO) o;
		return Objects.equals(value, that.value) &&
				Objects.equals(text, that.text);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value, text);
	}
}
