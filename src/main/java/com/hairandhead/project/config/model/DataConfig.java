package com.hairandhead.project.config.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class DataConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(unique = true, nullable = false)
    private String property;

    @Column(nullable = false)
    private String value;

    public DataConfig() {
        this.name = "";
        this.property = "";
        this.value = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataConfig that = (DataConfig) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(property, that.property) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, property, value);
    }
}