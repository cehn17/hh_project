package com.hairandhead.project.config.controller;

import com.hairandhead.project.config.model.DataConfig;
import com.hairandhead.project.config.service.DataConfigService;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/config")
public class DataConfigController {

    @Autowired
    private DataConfigService dataConfigService;

    @RequestMapping(value = { "/list" }, method = RequestMethod.GET)
    public String list(ModelMap model) {
        model.addAttribute("configList", dataConfigService.findAll());
        return "dataconfig/configlist";
    }

    @RequestMapping(value = { "/edit" }, method = RequestMethod.GET)
    public String editForm(@RequestParam int id, ModelMap model) {
        model.addAttribute("config", dataConfigService.findById(id));
        return "dataconfig/configform";
    }

    @RequestMapping(value = { "/edit" }, method = RequestMethod.POST)
    public String edit(DataConfig dataConfig) {
        dataConfigService.update(dataConfig);
        return "redirect:/config/list";
    }
    
    @RequestMapping(value = { "/restore" }, method = RequestMethod.GET)
	public String restoreDatabase() {
	
		try {
			Resource resource = new ClassPathResource("backup/recoveryScript.bat");
    		Resource carpeta = new ClassPathResource("backup/mysql.exe");
			Runtime.getRuntime().exec(resource.getFile().getAbsolutePath() +" " +carpeta.getFile().getAbsoluteFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "redirect:/config/list";
	}
}
