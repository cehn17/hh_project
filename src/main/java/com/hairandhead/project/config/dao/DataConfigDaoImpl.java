package com.hairandhead.project.config.dao;

import com.hairandhead.project.config.model.DataConfig;
import com.hairandhead.project.utils.AbstractDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("DataConfigDao")
public class DataConfigDaoImpl extends AbstractDao<Integer, DataConfig> implements DataConfigDao {

    @Override
    public DataConfig findById(int id) {
        return super.getByKey(id);
    }

    @Override
    public DataConfig findByProperty(String property) {
        Criteria criteria = super.createEntityCriteria();
        criteria.add(Restrictions.eq("property", property));
        return (DataConfig) criteria.uniqueResult();
    }

    @Override
    public List<DataConfig> findAll() {
        Criteria criteria = super.createEntityCriteria();
        return criteria.list();
    }

    @Override
    public void save(DataConfig dataConfig) {
        persist(dataConfig);
    }
}
