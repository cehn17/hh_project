package com.hairandhead.project.config.service;

import com.hairandhead.project.config.dao.DataConfigDao;
import com.hairandhead.project.config.model.DataConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service("dataConfigService")
public class DataConfigServiceImpl implements DataConfigService {

    @Autowired
    private DataConfigDao dataConfigDao;

    @Override
    public DataConfig findById(int id) {
        return dataConfigDao.findById(id);
    }

    @Override
    public DataConfig findByProperty(String property) {
        return dataConfigDao.findByProperty(property);
    }

    @Override
    public List<DataConfig> findAll() {
        return dataConfigDao.findAll();
    }

    @Override
    public void save(DataConfig dataConfig) {
        dataConfigDao.save(dataConfig);
    }

    @Override
    public void update(DataConfig dataConfig) {
        dataConfigDao.update(dataConfig);
    }

    @Override
    public void delete(DataConfig dataConfig) {
        dataConfigDao.delete(dataConfig);
    }
}
