package com.hairandhead.project.user.service;

import java.util.List;

import com.hairandhead.project.user.model.User;


public interface UserService {
	
	User findById(int id);
	
	User findBySSO(String sso);

	User findByEmail(String email);
	
	void saveUser(User user);
	
	void updateUser(User user);
	
	void deleteUserBySSO(String sso);

	List<User> findAllUsers(); 
	
	boolean isUserSSOUnique(Integer id, String sso);

	void deleteUser(String ssoid);

	void refreshPassword(String email);
}