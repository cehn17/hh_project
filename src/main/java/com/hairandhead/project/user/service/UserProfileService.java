package com.hairandhead.project.user.service;

import java.util.List;

import com.hairandhead.project.user.model.UserProfile;


public interface UserProfileService {

	UserProfile findById(int id);

	UserProfile findByType(String type);
	
	List<UserProfile> findAll();
	
}
