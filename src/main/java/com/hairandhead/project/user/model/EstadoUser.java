package com.hairandhead.project.user.model;

public enum EstadoUser {
    ACTIVO, INACTIVO;
}
