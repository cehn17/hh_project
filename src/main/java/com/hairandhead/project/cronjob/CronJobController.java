package com.hairandhead.project.cronjob;

import com.hairandhead.project.turno.model.EstadoTurno;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.TurnoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.PostConstruct;

import java.io.IOException;
import java.util.List;

@Controller
public class CronJobController {

    private static final Logger LOG = LoggerFactory.getLogger(CronJobController.class);

    @Autowired
    private TurnoService turnoService;

    @PostConstruct
    @Scheduled(cron = "0 */2 * ? * *")
    public void checkReservations(){
        turnoService.getExpiredTurnos().forEach(turno -> {
            turno.setEstadoTurno(EstadoTurno.AUSENTE);
            turnoService.updateTurno(turno);
            LOG.info("El turno ".concat(turno.getId().toString()).concat(" ha sido cancelado debido a que el cliente se ausento"));
        });
    }
    @PostConstruct
    @Scheduled(cron = "0 1 1 * * ?")
	public void Backupdbtosql() {
    	try {
    		Resource resource = new ClassPathResource("backup/script_backup.bat");
    		Resource carpeta = new ClassPathResource("backup/mysqldump.exe");
			Runtime.getRuntime().exec(resource.getFile().getAbsolutePath() +" "+carpeta.getFile().getAbsoluteFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}