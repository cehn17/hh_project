package com.hairandhead.project.turno.dao;

import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.utils.AbstractDao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("detalleTurnoDao")
public class DetalleTurnoDaoImpl extends AbstractDao<Integer, DetalleTurno> implements DetalleTurnoDao {

    @Override
    public DetalleTurno findById(int id) {
        return super.getByKey(id);
    }

    @Override
    public void save(DetalleTurno detalleTurno) {
        super.persist(detalleTurno);
    }

    @Override
    public void delete(DetalleTurno detalleTurno) {
        super.delete(detalleTurno);
    }

    @Override
    public List<DetalleTurno> findAllDetalleTurnos() {
        return null;
    }
    
    @SuppressWarnings("unchecked")
	public List<DetalleTurno> findAllDetalleTurnosPorProfesional(int idProfesional) {
    	Criteria criteria = super.createEntityCriteria();
		criteria.add(Restrictions.eq("profesional.id", idProfesional));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
		List<DetalleTurno> detalleTurnos = (List<DetalleTurno>) criteria.list();
		
		return detalleTurnos;
    }
}
