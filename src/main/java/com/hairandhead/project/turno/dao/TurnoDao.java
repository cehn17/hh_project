package com.hairandhead.project.turno.dao;

import java.util.List;

import com.hairandhead.project.turno.model.Turno;

public interface TurnoDao {
	Turno findById(int id);
	
	void save(Turno turno);
	
	void deleteById(int id);
	
	List<Turno> findAllTurnos();
	
	List<Turno> findAllTurnosReservados();
	
	Turno findTurnoByEmail(String email);

	void update(Turno turno);
}
