package com.hairandhead.project.turno.dao;

import com.hairandhead.project.turno.model.DetalleTurno;

import java.util.List;

public interface DetalleTurnoDao {
    DetalleTurno findById(int id);

    void save(DetalleTurno detalleTurno);

    void delete(DetalleTurno detalleTurno);

    void update(DetalleTurno detalleTurno);

    List<DetalleTurno> findAllDetalleTurnos();
    
    List<DetalleTurno> findAllDetalleTurnosPorProfesional(int idProfesional);
}
