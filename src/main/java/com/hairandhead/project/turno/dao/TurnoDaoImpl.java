package com.hairandhead.project.turno.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.EstadoTurno;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.utils.AbstractDao;

@Repository("turnoDao")
public class TurnoDaoImpl extends AbstractDao<Integer, Turno> implements TurnoDao {
	static final Logger logger = LoggerFactory.getLogger(TurnoDaoImpl.class);
	
	public Turno findById(int id) {
		Turno turno = getByKey(id);
		return turno;
	}

	@SuppressWarnings("unchecked")
	public List<Turno> findAllTurnos() {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("id"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
		List<Turno> turnos = (List<Turno>) criteria.list();
		
		return turnos;
	}

	@Override
	public Turno findTurnoByEmail(String email) {
		Criteria criteria = super.createEntityCriteria();
		criteria.add(Restrictions.eq("cliente.email", email));
		return (Turno) criteria.uniqueResult();
	}

	public void save(Turno user) {
		persist(user);
	}

	public void deleteById(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Turno turno = (Turno)crit.uniqueResult();
		delete(turno);
	}

	@Override
	public void update(Turno turno){
		super.update(turno);
	}

	@Override
	public List<Turno> findAllTurnosReservados() {
		List<Turno> ret = new ArrayList<Turno>();
		List<Turno> turnos = this.findAllTurnos();
		for(Turno turno : turnos ) {
			if(turno.getEstadoTurno().equals(EstadoTurno.RESERVADO)) {
				ret.add(turno);
			}
		}
		return ret;
	}

}
