package com.hairandhead.project.turno.dto;

import java.util.Objects;

public class EventoDTO {

	private Integer id;
	private String title;
	private String start;
	private String end;
	
    public EventoDTO() {
    	this.id = null;
    	this.title = "";
    	this.start = "";
    	this.end = "";
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EventoDTO eventoDTO = (EventoDTO) o;
		return Objects.equals(id, eventoDTO.id) &&
				Objects.equals(title, eventoDTO.title) &&
				Objects.equals(start, eventoDTO.start) &&
				Objects.equals(end, eventoDTO.end);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, title, start, end);
	}
}
