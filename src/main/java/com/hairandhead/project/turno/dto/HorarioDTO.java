package com.hairandhead.project.turno.dto;

import java.util.Objects;

public class HorarioDTO {
	private String horaInicio;
	private String horaFin;
	private String porcentaje;
	private boolean disponible;
	private boolean libre;

	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}
	public boolean isDisponible() {
		return disponible;
	}
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	public boolean isLibre() {
		return libre;
	}
	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		HorarioDTO that = (HorarioDTO) o;
		return disponible == that.disponible &&
				libre == that.libre &&
				Objects.equals(horaInicio, that.horaInicio) &&
				Objects.equals(horaFin, that.horaFin) &&
				Objects.equals(porcentaje, that.porcentaje);
	}

	@Override
	public int hashCode() {
		return Objects.hash(horaInicio, horaFin, porcentaje, disponible, libre);
	}
}
