package com.hairandhead.project.turno.model;

import javax.persistence.*;

import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.servicio.model.Servicio;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name="DETALLE_TURNO")
public class DetalleTurno implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    private Servicio servicio;

    @OneToOne
    private Profesional profesional;
    
	@Column
	private LocalTime horaInicio;

	@Column
	private LocalTime horaFin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Profesional getProfesional() {
        return profesional;
    }

    public void setProfesional(Profesional profesional) {
        this.profesional = profesional;
    }
    
	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}

	@Override
	public String toString() {
		return "DetalleTurno [id=" + id + ", servicio=" + servicio + ", profesional=" + profesional + ", horaInicio="
				+ horaInicio + ", horaFin=" + horaFin + "]";
	}


    
    
}
