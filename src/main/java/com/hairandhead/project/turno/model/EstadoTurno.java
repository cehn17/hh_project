package com.hairandhead.project.turno.model;

public enum EstadoTurno {
    BORRADOR, 
    RESERVADO,
    FINALIZADO, 
    AUSENTE,
    CANCELADO,
    RECEPCIONADO
}
