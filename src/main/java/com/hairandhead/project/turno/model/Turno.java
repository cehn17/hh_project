package com.hairandhead.project.turno.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.sucursal.model.Sucursal;

@Entity
@Table(name="TURNO")
public class Turno implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@OneToOne
	private Cliente cliente;

	@OneToOne
	private Sucursal sucursal;

	@Column(name="ID_ADMINISTRATIVO")
	private String administrativo;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<DetalleTurno> detalleTurnos;

	@Column(name="FECHA_ALTA")
	private LocalDateTime fechaAlta;
	
	@Column(name="FECHA")
	private LocalDate fecha;

	@Column(name="FECHA_INICIO")
	private LocalTime horaInicio;

	@Column(name="FECHA_FIN")
	private LocalTime horaFin;

	@OneToOne(cascade = CascadeType.ALL)
	private Pago pago;
	
	@Column
	private EstadoTurno estadoTurno;

	@ManyToMany(cascade = CascadeType.ALL)
	private List<PromocionAplicada> promociones;

	public Turno() {
		this.cliente = null;
		this.administrativo = "";
		this.detalleTurnos = new ArrayList<>();
		this.fechaAlta = LocalDateTime.now();
		this.fecha = null;
		this.horaInicio = null;
		this.horaFin = null;
		this.pago = null;
		this.sucursal = null;
		this.estadoTurno = EstadoTurno.RESERVADO;
		this.promociones = new ArrayList<>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getAdministrativo() {
		return administrativo;
	}

	public void setAdministrativo(String administrativo) {
		this.administrativo = administrativo;
	}

	public LocalDateTime getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDateTime fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public List<DetalleTurno> getDetalleTurnos() {
		return detalleTurnos;
	}

	public void setDetalleTurnos(List<DetalleTurno> detalleTurnos) {
		this.detalleTurnos = detalleTurnos;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}

	public Pago getPago() {
		return this.pago;
	}

	public void setPagos(Pago pago) {
		this.pago = pago;
	}

	public EstadoTurno getEstadoTurno() {
		return estadoTurno;
	}

	public void setEstadoTurno(EstadoTurno estadoTurno) {
		this.estadoTurno = estadoTurno;
	}

	public List<PromocionAplicada> getPromociones() {
		return promociones;
	}

	public void setPromociones(List<PromocionAplicada> promociones) {
		this.promociones = promociones;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	@Override
	public String toString() {
		return "Turno [id=" + id + ", cliente=" + cliente + ", administrativo=" + administrativo + ", detalleTurnos="
				+ detalleTurnos + ", fechaAlta=" + fechaAlta + ", fecha=" + fecha + ", horaInicio=" + horaInicio
				+ ", horaFin=" + horaFin + ", pago=" + pago + ", sucursal=" + sucursal + ", estadoTurno=" + estadoTurno
				+ ", promociones=" + promociones + "]";
	}



}
