package com.hairandhead.project.turno.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.swing.JOptionPane;
import javax.validation.Valid;

import com.hairandhead.project.cliente.dto.ClienteJsonDTO;
import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.service.ClienteService;
import com.hairandhead.project.pago.dto.PagoDTO;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.pago.service.DetallePagoService;
import com.hairandhead.project.pago.service.PagoService;
import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.profesional.service.ProfesionalService;
import com.hairandhead.project.promocion.dto.PromocionDTO;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import com.hairandhead.project.promocion.service.PromocionAplicadaService;
import com.hairandhead.project.promocion.service.PromocionService;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.sucursal.service.SucursalService;
import com.hairandhead.project.turno.dto.TurnoFrontDTO;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.EstadoTurno;
import com.hairandhead.project.turno.model.Horario;
import com.hairandhead.project.turno.service.DetalleTunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hairandhead.project.servicio.dto.ServicioJsonDTO;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.turno.dto.EventoDTO;
import com.hairandhead.project.turno.dto.HorarioDTO;
import com.hairandhead.project.turno.dto.TurnoClienteDTO;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.TurnoService;

@Controller
@RequestMapping({ "/turnos", "/" })
public class AppControllerTurno {

	@Autowired
	private ProfesionalService profesionalService;

	@Autowired
	TurnoService turnoService;

	@Autowired
	ClienteService clienteService;
	
	@Autowired
	SucursalService sucursalService;

	@Autowired
	private ServicioService servicioService;

	@Autowired
	private DetalleTunoService detalleTunoService;

	@Autowired
	private PromocionService promocionService;
	
	@Autowired
	private PagoService	pagoService;
	
	@Autowired
	private DetallePagoService	detallePagoService;

	@Autowired
	private PromocionAplicadaService promocionAplicadaService;

	@Autowired
	MessageSource messageSource;

	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;

	/**
	 * This method will list all existing users.
	 */
	@RequestMapping(value = { "/calendar" }, method = RequestMethod.GET)
	public String listTurnos(ModelMap model) {
		return "/turnos/calendar";
	}

	@RequestMapping(value = { "/list" }, method = RequestMethod.GET)
	public ResponseEntity<List<EventoDTO>> list() {

		List<EventoDTO> turnoDto = new ArrayList<>();
		List<Turno> turnoList = turnoService.findAllTurnos();
		for (Turno s : turnoList) {
			turnoDto.addAll(turnoService.convertTurnoToEventDTO(s));
		}

		return new ResponseEntity<>(turnoDto, HttpStatus.OK);
	}

	@RequestMapping(value = { "/", "/listarturnos" }, method = RequestMethod.GET)
	public String listAllTurnos(ModelMap model) {
		List<TurnoFrontDTO> turnoFrontDTOList = new ArrayList<>();
		turnoService.findAllTurnos().forEach(turno -> {
			turnoFrontDTOList.add(turnoService.convertToTurnoFrontDTO(turno));
		});
		model.addAttribute("turnoListFrontDTO", turnoFrontDTOList);
		return "/turnos/listTurnoFrontDTO";
	}

	/**
	 * This method will provide the medium to add a new user.
	 */
	@RequestMapping(value = { "/nuevoturno" }, method = RequestMethod.GET)
	public String newTurno(ModelMap model) {
		TurnoClienteDTO turnoClienteDTO = new TurnoClienteDTO();
		model.addAttribute("turnoClienteDTO", turnoClienteDTO);
		return "/turnos/newregistrationTurno";
	}

	@RequestMapping(value = { "/getClientes" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<ClienteJsonDTO>> getClientes() {
		List<ClienteJsonDTO> clienteJsonList = new ArrayList<ClienteJsonDTO>();
		ClienteJsonDTO clienteJson = null;
		List<Cliente> clientes = clienteService.findAllClientesActivos();
		for (Cliente s : clientes) {
			clienteJson = new ClienteJsonDTO();
			clienteJson.setValue(s.getId());
			clienteJson.setText(s.getNombre() + " " + s.getApellido() + " - " + s.getEmail());

			clienteJsonList.add(clienteJson);
		}
		return new ResponseEntity<List<ClienteJsonDTO>>(clienteJsonList, HttpStatus.OK);
	}

	@RequestMapping(value = { "/registrarturno" }, method = RequestMethod.GET)
	public String createTurno(TurnoClienteDTO turnoClienteDTO, ModelMap model) {
		Cliente cliente = clienteService.findClienteById(Integer.parseInt(turnoClienteDTO.getCliente()));
		Sucursal sucursal = sucursalService.buscarPorId(Integer.parseInt(turnoClienteDTO.getSucursal()));
		
		Turno turno = new Turno();
		turno.setCliente(cliente);
		turno.setSucursal(sucursal);
		turno.setEstadoTurno(EstadoTurno.BORRADOR);
		try {
			turno.setFecha(LocalDate.parse(turnoClienteDTO.getFecha(), DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		turnoService.saveTurno(turno);
		model.addAttribute("turnoId", turno.getId());
		return "redirect:/turnos/verdetalle";
	}

	public List<Turno> turnosDelProfesionalPorDia(Integer profesionalId, String fecha) {
		LocalDate fechaSolicitada = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		List<Turno> turnosDelProfesional = new ArrayList<Turno>();

		for (Turno t : turnoService.findAllTurnos()) {
			for (DetalleTurno dt : t.getDetalleTurnos()) {
				if (dt.getProfesional() != null && dt.getProfesional().getId() == profesionalId) {
					turnosDelProfesional.add(t);
				}
			}
		}

		List<Turno> turnosDelDia = new ArrayList<Turno>();
		for (Turno t : turnosDelProfesional) {
			if (t.getFecha().equals(fechaSolicitada)) {
				boolean bandera = true;
				for (Turno norepetido : turnosDelDia) {
					if (t.getId().equals(norepetido.getId())) {
						bandera = false;
					}
				}
				if (bandera) {
					turnosDelDia.add(t);
				}
			}
		}

		return turnosDelDia;
	}

	public HashMap<Integer, String> diasDeLaSemana() {
		HashMap<Integer, String> ret = new HashMap<>();
		ret.put(1, "LUNES");
		ret.put(2, "MARTES");
		ret.put(3, "MIERCOLES");
		ret.put(4, "JUEVES");
		ret.put(5, "VIERNES");
		ret.put(6, "SABADO");
		ret.put(7, "DOMINGO");

		return ret;
	}

	public List<Horario> construirFranjaHoraria(LocalTime inicio, LocalTime fin, List<Turno> turnosDelDia) {
		List<Horario> turnosDisponibles = new ArrayList<>();
		System.out.println("Franja: " + inicio + " - " + fin);
		// Caso base, tenes toda la franja
		if (turnosDelDia.size() == 0) {
			System.out.println("Entre al 1 caso base");
			Horario h = new Horario(inicio, fin);
			h.setDisponible(true);
			turnosDisponibles.add(h);
			return turnosDisponibles;
		}

		// Me quedo con los horarios no disponibles de la franja
		List<Horario> horariosNoDisponibles = new ArrayList<Horario>();
		for (Turno t1 : turnosDelDia) {
			for (DetalleTurno t : t1.getDetalleTurnos()) {
				Horario horarioNoDisponible = new Horario(t.getHoraInicio(), t.getHoraFin());
				Horario base = new Horario(inicio, fin);
				// Condicion h1 < h2 < h3
				if ((horarioNoDisponible.getHoraInicio().compareTo(base.getHoraInicio()) == 0
						|| horarioNoDisponible.getHoraInicio().compareTo(base.getHoraInicio()) > 0)
						&& (horarioNoDisponible.getHoraFin().compareTo(base.getHoraFin()) < 0
								|| horarioNoDisponible.getHoraFin().compareTo(base.getHoraFin()) == 0)) {
					horarioNoDisponible.setDisponible(false);
					horariosNoDisponibles.add(horarioNoDisponible);
				}
			}
		}

		if (horariosNoDisponibles.size() == 0) {
			Horario h = new Horario(inicio, fin);
			h.setDisponible(true);
			turnosDisponibles.add(h);
			return turnosDisponibles;
		}

		Collections.sort(horariosNoDisponibles);

		// List<Horario> turnosDisponibles = new ArrayList<Horario>();
		// Franja inicial
		if (!horariosNoDisponibles.isEmpty() && inicio.compareTo(horariosNoDisponibles.get(0).getHoraInicio()) != 0) {
			Horario primerHorario = new Horario(inicio, horariosNoDisponibles.get(0).getHoraInicio());
			primerHorario.setDisponible(true);
			turnosDisponibles.add(primerHorario);
		}
		// Franjas intermedias

		for (int i = 0; i < horariosNoDisponibles.size(); i++) {
			LocalTime horaInicio = horariosNoDisponibles.get(i).getHoraFin();
			if (i != horariosNoDisponibles.size() - 1
					&& horaInicio.compareTo(horariosNoDisponibles.get(i + 1).getHoraInicio()) != 0) {
				LocalTime horaFin = horariosNoDisponibles.get(i + 1).getHoraInicio();
				Horario disponible = new Horario(horaFin, horaInicio);
				disponible.setDisponible(true);

				turnosDisponibles.add(horariosNoDisponibles.get(i));
				turnosDisponibles.add(disponible);
			} else {
				turnosDisponibles.add(horariosNoDisponibles.get(i));
			}
		}
		// Franja final
		if (!horariosNoDisponibles.isEmpty()
				&& fin.compareTo(horariosNoDisponibles.get(horariosNoDisponibles.size() - 1).getHoraFin()) != 0) {
			Horario ultimoHorario = new Horario(
					horariosNoDisponibles.get(horariosNoDisponibles.size() - 1).getHoraFin(), fin);
			ultimoHorario.setDisponible(true);
			turnosDisponibles.add(ultimoHorario);
		}

		return turnosDisponibles;

	}

	@RequestMapping(value = { "/getHorariosDisponibles" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<HorarioDTO>> getHorariosDisponibles(@RequestParam Integer profesionalId,
			@RequestParam String fecha) {
		// Filtro los turnos del dia para el profesional elegido
		Profesional p = profesionalService.findById(profesionalId);
		LocalDate fechaSolicitada = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		List<Turno> turnosDelDia = turnosDelProfesionalPorDia(profesionalId, fecha);

		HashMap<Integer, String> diasDeLaSemana = diasDeLaSemana();
		String dia = diasDeLaSemana.get(fechaSolicitada.getDayOfWeek().getValue());
		Disponibilidad disponibilidad = new Disponibilidad();
		for (Disponibilidad d : p.getDisponibilidad()) {
			if (d.getDia().equals(dia)) {
				disponibilidad = d;
			}
		}
		// Puede tener n horarios partidos
		LinkedList<Horario> franjaHoraria = new LinkedList<>();
		for (int i = 0; i < disponibilidad.getHoraDisponible().size(); i++) {
			LocalTime horaInicio = disponibilidad.getHoraDisponible().get(i).getHoraInicio();
			LocalTime horaFin = disponibilidad.getHoraDisponible().get(i).getHoraFin();
			franjaHoraria.addAll(construirFranjaHoraria(horaInicio, horaFin, turnosDelDia));
			if (i != disponibilidad.getHoraDisponible().size() - 1) {
				Horario horaLibre = new Horario(horaFin, disponibilidad.getHoraDisponible().get(i + 1).getHoraInicio());
				horaLibre.setLibre(true);
				horaLibre.setDisponible(false);
				franjaHoraria.add(horaLibre);
			}
		}

		List<HorarioDTO> horarioDTOList = new ArrayList<>();
		for (Horario h : franjaHoraria) {
			HorarioDTO horarioDTO = turnoService.convertToHorarioDTO(h);
			horarioDTOList.add(horarioDTO);
		}

		return new ResponseEntity<List<HorarioDTO>>(horarioDTOList, HttpStatus.OK);
	}

	public List<Horario> construirFranjaHorariaDisponible(LocalTime inicio, LocalTime fin, List<Turno> turnosDelDia) {
		List<Horario> turnosDisponibles = new ArrayList<>();

		// Caso base, tenes toda la franja
		if (turnosDelDia.size() == 0) {
			Horario h = new Horario(inicio, fin);
			h.setDisponible(true);
			turnosDisponibles.add(h);
			return turnosDisponibles;
		}
		// Me quedo con los horarios no disponibles de la franja
		List<Horario> horariosNoDisponibles = new ArrayList<Horario>();
		for (Turno t : turnosDelDia) {
			Horario horarioNoDisponible = new Horario(t.getHoraInicio(), t.getHoraFin());
			Horario base = new Horario(inicio, fin);
			// Condicion h1 < h2 < h3
			if ((horarioNoDisponible.getHoraInicio().compareTo(base.getHoraInicio()) == 0
					|| horarioNoDisponible.getHoraInicio().compareTo(base.getHoraInicio()) > 0)
					&& (horarioNoDisponible.getHoraFin().compareTo(base.getHoraFin()) < 0
							|| horarioNoDisponible.getHoraFin().compareTo(base.getHoraFin()) == 0)) {
				horarioNoDisponible.setDisponible(false);
				horariosNoDisponibles.add(horarioNoDisponible);
			}
		}
		// Caso base 2 no tenes horarios ocupados. Tenes toda la franja
		if (horariosNoDisponibles.size() == 0) {
			Horario h = new Horario(inicio, fin);
			h.setDisponible(true);
			turnosDisponibles.add(h);
			return turnosDisponibles;
		}

		Collections.sort(horariosNoDisponibles);

		// Franja inicial
		if (!horariosNoDisponibles.isEmpty() && inicio.compareTo(horariosNoDisponibles.get(0).getHoraInicio()) != 0) {
			Horario primerHorario = new Horario(inicio, horariosNoDisponibles.get(0).getHoraInicio());
			primerHorario.setDisponible(true);
			turnosDisponibles.add(primerHorario);
		}
		// Franjas intermedias

		for (int i = 0; i < horariosNoDisponibles.size(); i++) {
			LocalTime horaInicio = horariosNoDisponibles.get(i).getHoraFin();
			if (i != horariosNoDisponibles.size() - 1
					&& horaInicio.compareTo(horariosNoDisponibles.get(i + 1).getHoraInicio()) != 0) {
				LocalTime horaFin = horariosNoDisponibles.get(i + 1).getHoraInicio();
				Horario disponible = new Horario(horaFin, horaInicio);
				disponible.setDisponible(true);
				turnosDisponibles.add(disponible);
			}
		}
		// Franja final
		if (!horariosNoDisponibles.isEmpty()
				&& fin.compareTo(horariosNoDisponibles.get(horariosNoDisponibles.size() - 1).getHoraFin()) != 0) {
			Horario ultimoHorario = new Horario(
					horariosNoDisponibles.get(horariosNoDisponibles.size() - 1).getHoraFin(), fin);
			ultimoHorario.setDisponible(true);
			turnosDisponibles.add(ultimoHorario);
		}

		return turnosDisponibles;

	}

	public List<Horario> getHorarios(@RequestParam Integer profesionalId, @RequestParam String fecha) {
		Profesional p = profesionalService.findById(profesionalId);
		LocalDate fechaSolicitada = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		List<Turno> turnosDelDia = turnosDelProfesionalPorDia(profesionalId, fecha);
		HashMap<Integer, String> diasDeLaSemana = diasDeLaSemana();
		String dia = diasDeLaSemana.get(fechaSolicitada.getDayOfWeek().getValue());
		Disponibilidad disponibilidad = new Disponibilidad();
		for (Disponibilidad d : p.getDisponibilidad()) {
			if (d.getDia().equals(dia)) {
				disponibilidad = d;
			}
		}
		LinkedList<Horario> franjaHorariaDisponible = new LinkedList<>();
		for (int i = 0; i < disponibilidad.getHoraDisponible().size(); i++) {
			LocalTime horaInicio = disponibilidad.getHoraDisponible().get(i).getHoraInicio();
			LocalTime horaFin = disponibilidad.getHoraDisponible().get(i).getHoraFin();
			franjaHorariaDisponible.addAll(construirFranjaHorariaDisponible(horaInicio, horaFin, turnosDelDia));
		}
		return franjaHorariaDisponible;
	}

	public Horario horarioNoLaboral(@RequestParam Integer profesionalId, @RequestParam String fecha) {
		Profesional p = profesionalService.findById(profesionalId);
		LocalDate fechaSolicitada = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		HashMap<Integer, String> diasDeLaSemana = diasDeLaSemana();
		String dia = diasDeLaSemana.get(fechaSolicitada.getDayOfWeek().getValue());
		Disponibilidad disponibilidad = new Disponibilidad();
		for (Disponibilidad d : p.getDisponibilidad()) {
			if (d.getDia().equals(dia)) {
				disponibilidad = d;
			}
		}
		Horario diaLaboral = new Horario(
				disponibilidad.getHoraDisponible().get(disponibilidad.getHoraDisponible().size() - 1).getHoraFin(),
				disponibilidad.getHoraDisponible().get(0).getHoraInicio());

		return diaLaboral;
	}

	@RequestMapping(value = { "/nuevareserva" }, method = RequestMethod.POST)
	public String createReserva(@RequestParam int service, @RequestParam int professional, @RequestParam int turnoId,
			@RequestParam String hora, ModelMap model, RedirectAttributes attributes) {

		Turno turno = turnoService.findById(turnoId);
		Profesional profesional = profesionalService.findById(professional);
		Servicio servicio = servicioService.findServicioById(service);
		List<DetalleTurno> detalleTurnoList = turno.getDetalleTurnos();

		List<Horario> horariosDisponibles = getHorarios(professional,
				turno.getFecha().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

		LocalTime horaInicio = LocalTime.parse(hora);
		LocalTime horaFin = LocalTime.from(horaInicio).plusMinutes(servicio.getTiempoPromedioDeDuracion().toMinutes());
		Horario horario = new Horario(horaInicio, horaFin);

		boolean valido = false;
		Horario horarioNoLaboral = horarioNoLaboral(professional,
				turno.getFecha().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		for (Horario fhoraria : horariosDisponibles) {
			// Tiene que estar dentro de lo que el profesional trabaja, y en un horario
			// disponible
			if ((!((horario.getHoraInicio().compareTo(horarioNoLaboral.getHoraInicio()) == 0
					|| horario.getHoraInicio().compareTo(horarioNoLaboral.getHoraInicio()) > 0)
					&& (horario.getHoraFin().compareTo(horarioNoLaboral.getHoraFin()) == 0
							|| horario.getHoraFin().compareTo(horarioNoLaboral.getHoraFin()) < 0))
					&& (horario.getHoraInicio().compareTo(fhoraria.getHoraInicio()) == 0
							|| horario.getHoraInicio().compareTo(fhoraria.getHoraInicio()) > 0)
					&& (horario.getHoraFin().compareTo(fhoraria.getHoraFin()) == 0
							|| horario.getHoraFin().compareTo(fhoraria.getHoraFin()) < 0))) {
				valido = true;
			}
		}

		String msg = "";
		//Horarios contiguos
		if (turno.getDetalleTurnos().size() != 0) {
			LocalTime horarioUltimoDT = turno.getDetalleTurnos().get(turno.getDetalleTurnos().size()-1).getHoraFin();
			if(!(horario.getHoraInicio().compareTo(horarioUltimoDT) == 0)){
				valido = false;
				msg = "El horario de los servicios debe ser contiguo";
			}
		}
		if (valido) {
			DetalleTurno detalleTurno = new DetalleTurno();
			detalleTurno.setServicio(servicio);
			detalleTurno.setProfesional(profesional);
			detalleTurnoList.add(detalleTurno);
			detalleTurno.setHoraInicio(horaInicio);
			detalleTurno.setHoraFin(horaFin);

			turno.setDetalleTurnos(detalleTurnoList);

			if (turno.getHoraInicio() != null && turno.getHoraFin() != null) {
				turno.setHoraFin(turno.getHoraFin().plusMinutes(servicio.getTiempoPromedioDeDuracion().toMinutes()));
			} else {
				turno.setHoraInicio(horaInicio);
				turno.setHoraFin(horaFin);
			}
			List<Promocion> promociones = new ArrayList<>(turnoService.getPromotionsAvailable(turno));
			if (!promociones.isEmpty()) {
				Collections.sort(promociones);
				turno.getPromociones().add(promocionService.applyPromotion(promociones.get(0)));
			}
				
			turnoService.updateTurno(turno);

			attributes.addAttribute("turnoId", turnoId);
			return "redirect:/turnos/verdetalle";
		} else {
		
			if(msg.equals( "")) {
				msg = "El horario ingresado no esta disponible";
			}
			attributes.addFlashAttribute("msgError", true);
			attributes.addFlashAttribute("msg", msg);
			
			attributes.addAttribute("turnoId", turnoId);
			
			return "redirect:/turnos/verdetalle";
		}

	}

	@RequestMapping(value = { "/eliminarreserva" }, method = RequestMethod.GET)
	public String deleteReserva(@RequestParam int idTurno, @RequestParam int idReserva, ModelMap model) {
		Turno turno = turnoService.findById(idTurno);
		List<DetalleTurno> detalleTurnoList = turno.getDetalleTurnos();
		Iterator<DetalleTurno> it = detalleTurnoList.iterator();

		while (it.hasNext()) {
			DetalleTurno dt = it.next();
			if (dt.getId().equals(idReserva)) {
				it.remove();
			}
		}

		turno.setDetalleTurnos(detalleTurnoList);
		turnoService.updateTurno(turno);
		detalleTunoService.delete(idReserva);
		model.addAttribute("turnoId", idTurno);
		return "redirect:/turnos/verdetalle";
	}

	@RequestMapping(value = { "/editar" }, method = RequestMethod.POST)
	public String edit(@Valid TurnoFrontDTO turnoFrontDTO, ModelMap model) {
		Turno turno = turnoService.findById(Integer.parseInt(turnoFrontDTO.getTurnoId()));
		turno.setEstadoTurno(turnoFrontDTO.getStatus() != null && "RESERVADO".equals(turnoFrontDTO.getStatus())
				? EstadoTurno.RESERVADO
				: EstadoTurno.CANCELADO);
		turnoService.updateTurno(turno);
		model.addAttribute("turnoId", turnoFrontDTO.getTurnoId());
		return "/turnos/resumenturno";
	}

	@RequestMapping(value = { "/save" }, method = RequestMethod.POST)
	public String save(@Valid TurnoFrontDTO turnoFrontDTO, ModelMap model) {
		Turno turno = turnoService.findById(Integer.parseInt(turnoFrontDTO.getTurnoId()));
		turno.setEstadoTurno(EstadoTurno.valueOf(turnoFrontDTO.getStatus()));
		if(turnoFrontDTO.getStatus().equals("BORRADOR")){
			if(turno.getDetalleTurnos().size() > 0) {
				turno.setEstadoTurno(EstadoTurno.RESERVADO);
				Pago p = new Pago();
				boolean agregoPromo = false;
				boolean aplicoPromo = false;
				for (DetalleTurno dt : turno.getDetalleTurnos()) {
					for (PromocionAplicada promo : turno.getPromociones()) {
						for (ServicioPromocionAplicada serv : promo.getServicios()) {

							if (dt.getServicio().getId().equals(serv.getId()) && !aplicoPromo) {
								p.setMontoTotal(p.getMontoTotal()
										.add(dt.getServicio().getPrecio().subtract(dt.getServicio().getPrecio()
												.multiply(promo.getDescuento().divide(new BigDecimal(100))))));
								agregoPromo = true;
								aplicoPromo = true;
							}
						}
					}
					if (!agregoPromo)
						p.setMontoTotal(
								new BigDecimal(p.getMontoTotal().add(dt.getServicio().getPrecio()).floatValue()));
					agregoPromo = false;
					aplicoPromo = false;
				}
				p.setMonto(p.getMontoTotal());
				p.setMontoAbonado(new BigDecimal(0));
				turno.setPago(p);
			}
			else {
				model.addAttribute("turnoId", turno.getId());
				return "redirect:/turnos/verdetalle";
			}
		}

		turnoService.updateTurno(turno);
		return "redirect:/turnos/";
	}

	@RequestMapping(value = { "/verdetalle" }, method = RequestMethod.GET)
	public String showDetails(@RequestParam int turnoId, ModelMap model) {
		Turno turno = turnoService.findById(turnoId);
		model.addAttribute("promosDisponibles",
				promocionService.convertToDTOList(new ArrayList<>(turnoService.getPromotionsAvailable(turno))));
		model.addAttribute("promosAplicadas", promocionAplicadaService.convertPromocionAplicadaToDTOList(turno.getPromociones()));
		model.addAttribute("turnoFrontDTO", turnoService.convertToTurnoFrontDTO(turno));
		model.addAttribute("detalleTurnoList", turnoService.convertToDetalleTurnoFrontDTOList(turno));
		return "/turnos/resumenturno";
	}

	@RequestMapping(value = { "/agregarPromo" }, method = RequestMethod.GET)
	public String addPromo(@RequestParam int turnoId, @RequestParam int promoId, ModelMap model) {
		Turno turno = turnoService.findById(turnoId);
		Promocion promo = promocionService.findPromocionById(promoId);
		PromocionAplicada appliedPromotion = promocionService.applyPromotion(promo);
		promocionAplicadaService.savePromocionAplicada(appliedPromotion);

		List<PromocionAplicada> promocionList = turno.getPromociones();
		promocionList.add(appliedPromotion);
		turno.setPromociones(promocionList);
		turnoService.updateTurno(turno);

		model.addAttribute("turnoId", turno.getId());
		return "redirect:/turnos/verdetalle";
	}

	@RequestMapping(value = { "/eliminarPromo" }, method = RequestMethod.GET)
	public String deletePromo(@RequestParam int turnoId, @RequestParam int promoId, ModelMap model) {
		Turno turno = turnoService.findById(turnoId);
		List<PromocionAplicada> promocionList = turno.getPromociones();

		Iterator<PromocionAplicada> it = promocionList.iterator();
		while (it.hasNext()) {
			PromocionAplicada p = it.next();
			if (p.getId().equals(promoId)) {
				it.remove();
			}
		}
		turno.setPromociones(promocionList);
		turnoService.updateTurno(turno);

		model.addAttribute("turnoId", turno.getId());
		return "redirect:/turnos/verdetalle";
	}

	@RequestMapping(value = { "/obtenerservicios" }, method = RequestMethod.GET)
	public ResponseEntity<List<ServicioJsonDTO>> listServiciosJson() {
		List<ServicioJsonDTO> servicioJsonList = new ArrayList<ServicioJsonDTO>();
		ServicioJsonDTO servicioJson = null;
		List<Servicio> servicios = servicioService.findAllServiciosActivos();
		for (Servicio s : servicios) {
			servicioJson = new ServicioJsonDTO();
			servicioJson.setValue(s.getId());
			servicioJson.setText(s.getNombre());

			servicioJsonList.add(servicioJson);
		}

		return new ResponseEntity<List<ServicioJsonDTO>>(servicioJsonList, HttpStatus.OK);
	}
	
	@RequestMapping(value = { "/obtenerComprobante/{id}/{esPago}" }, method = RequestMethod.GET)
	public String mostrarComprobante(@PathVariable("id") int idTurno,@PathVariable("esPago") boolean esPago, ModelMap model) {
		
		Turno turno = turnoService.findById(idTurno);
		System.out.println(turnoService.convertToTurnoFrontDTO(turno));
		PagoDTO pagoDTO = new PagoDTO();
		pagoService.convertToDTO(turno.getPago(), pagoDTO);
		List<PromocionDTO> promocion = promocionService.convertToDTOList(new ArrayList<>(turnoService.getPromotionsAvailable(turno)));
		
		model.addAttribute("promosDisponibles",
				promocionService.convertToDTOList(new ArrayList<>(turnoService.getPromotionsAvailable(turno))));
		model.addAttribute("promosAplicadas", promocion);
		model.addAttribute("turnoFrontDTO", turnoService.convertToTurnoFrontDTO(turno));
		model.addAttribute("detalleTurnoList", turnoService.convertToDetalleTurnoFrontDTOList(turno));
		model.addAttribute("pagoDTO", pagoDTO);
		model.addAttribute("detallePagoDTO", detallePagoService.convertDetallePagoToDTOList(turno.getPago()));
		model.addAttribute("esPago", esPago);
		model.addAttribute("sinPromos", promocion.isEmpty());
	
		return "/turnos/comprobanteTurno";
	}
	
	@RequestMapping(value = { "turnos/enviarComprobante" }, method = RequestMethod.POST)
	public String sendReceipt(@RequestParam("file") MultipartFile file,@RequestParam("turnoId") int idTurno ) throws IOException {
		Turno turno = turnoService.findById(idTurno);
	    if (!file.isEmpty()) {
            File comprobante = new File(file.getOriginalFilename());
            file.transferTo(comprobante);
            turnoService.sendReceipt(turno.getId(),comprobante);
            comprobante.delete();
	    }
	    return "/turnos/calendar";
	}
	
	
}