package com.hairandhead.project.sucursal.service;

import java.util.List;

import com.hairandhead.project.sucursal.dto.SucursalDTO;
import com.hairandhead.project.sucursal.model.Sucursal;

public interface SucursalService {
	void guardar(Sucursal sucursal);
	void update(Sucursal sucursal);
	List<Sucursal> obtenerSucursales();
	List<Sucursal> obtenerSucursalesActivas();
	Sucursal buscarPorId(Integer id);
	boolean convertToSucursal(SucursalDTO sucursalDTO, Sucursal sucursal);
	boolean convertToDto(Sucursal sucursal, SucursalDTO sucursalDTO);
}
