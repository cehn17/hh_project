package com.hairandhead.project.sucursal.model;

import java.io.Serializable;

public enum EstadoSucursal implements Serializable{
	ALTA("ALTA"),
	BAJA("BAJA");
	
	String estadoSucursal;
	
	private EstadoSucursal(String userProfileType){
		this.estadoSucursal = userProfileType;
	}
}
	