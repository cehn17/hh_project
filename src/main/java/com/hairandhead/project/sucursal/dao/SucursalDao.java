package com.hairandhead.project.sucursal.dao;

import java.util.List;

import com.hairandhead.project.sucursal.model.Sucursal;

public interface SucursalDao {
	void guardar(Sucursal sucursal);
	void update(Sucursal sucursal);
	List<Sucursal> obtenerSucursales();
	List<Sucursal> obtenerSucursalesActivas();
	Sucursal buscarPorId(Integer id);
}
