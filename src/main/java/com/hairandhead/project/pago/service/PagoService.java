package com.hairandhead.project.pago.service;

import java.util.List;

import com.hairandhead.project.pago.dto.PagoDTO;
import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.Pago;

import net.sf.jasperreports.engine.JasperPrint;

public interface PagoService {
	
	Pago buscarPorId(int id);
	
	void guardar(Pago user);
	
	List<Pago> obtenerPagos();
	
	void editar(Pago p);

	boolean convertToPago(PagoDTO pagoDTO, Pago pago);

	void convertToDTO(Pago buscarPorId, PagoDTO dto);
	
	//boolean agregarDetalleDePago(Pago pago, DetallePago detallePago);

	JasperPrint exportPagoPdf(int idPago, int idTurno);
	
//	void generarComprobante();
	
}
