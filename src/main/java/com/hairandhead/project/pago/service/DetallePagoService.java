package com.hairandhead.project.pago.service;

import java.util.List;

import com.hairandhead.project.pago.dto.DetallePagoDTO;
import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.Pago;

public interface DetallePagoService {
    DetallePago findById(int id);

    void save(DetallePago detallePago);

    void delete(DetallePago detallePago);

    void update(DetallePago detallePago);
    
    void convertToDTO(DetallePago detallePago, DetallePagoDTO detallePagoDTO);
    
    void convertToDetallePago(DetallePago detallePago, DetallePagoDTO detallePagoDTO);

	List<DetallePagoDTO> convertDetallePagoToDTOList(Pago pago);

}
