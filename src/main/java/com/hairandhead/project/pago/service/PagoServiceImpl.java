package com.hairandhead.project.pago.service;

import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import com.hairandhead.project.reportes.service.Reporte;

import org.springframework.transaction.annotation.Transactional;

import com.hairandhead.project.pago.dao.PagoDao;
import com.hairandhead.project.pago.dto.DetallePagoDTO;
import com.hairandhead.project.pago.dto.PagoDTO;
import com.hairandhead.project.pago.dto.PagoReporte;
import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.EstadoPago;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.pago.model.TipoDetallePago;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.TurnoService;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("pagoService")
@Transactional
public class PagoServiceImpl implements PagoService{
	@Autowired
	private PagoDao dao;
	
	@Autowired
	private TurnoService turnoService;
	@Autowired
	private ServicioService servicioService;
	@Autowired
	private DetallePagoService detallePagoService;
	
	@Override
	public Pago buscarPorId(int id) {
		return dao.buscarPorId(id);
	}

	@Override
	public void guardar(Pago user) {
		dao.guardar(user);
	}

	@Override
	public List<Pago> obtenerPagos() {
		return dao.obtenerPagos();
	}
	
	public void editar(Pago p) {
		dao.editar(p);
	}

	@Override
	public boolean convertToPago(PagoDTO pagoDTO, Pago pago) {
		pago.setId(pagoDTO.getId());
		pago.setEstado(pagoDTO.getEstado().equals("PENDIENTE")?EstadoPago.PENDIENTE:EstadoPago.PAGADO);
		return true;
	}

	@Override
	public void convertToDTO(Pago pago, PagoDTO dto) {
		DecimalFormat df = new DecimalFormat();
		dto.setEstado(pago.getEstado() != null? pago.getEstado().name():"");
		dto.setFecha(pago.getFecha()!=null?new SimpleDateFormat("dd/MM/yyyy HH:mm").format(pago.getFecha()):"");
		dto.setId(pago.getId());
		dto.setMontoTotal(pago.getMontoTotal()!=null?df.format(pago.getMontoTotal()):"");
	}

	public PagoReporte convertToPagoReporte(Pago pago, Turno turno) {
		System.out.println("Conversion al reporte");
		PagoReporte rep = new PagoReporte();
		try {
			rep.setId(pago.getId());
			rep.setIdTurno(turno.getId());
			rep.setMonto(pago.getMonto().floatValue());
			rep.setMontoAbonado(pago.getMontoAbonado().floatValue());
			rep.setMontoTotal(pago.getMontoTotal().floatValue());
			rep.setEstado(pago.getEstado().name());
			//Ver
			//rep.setFecha(pago.getFecha().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			
			ArrayList<ServicioDTO> servicios = new ArrayList<ServicioDTO>();
			for(DetalleTurno dt: turno.getDetalleTurnos()) {
				ServicioDTO svDTO = servicioService.convertToDTO(dt.getServicio());
				servicios.add(svDTO);
			}
			rep.setServicios(servicios);
			
			
			
			if(turno.getPromociones().size() != 0) {
				String descripcion = "";
				Iterator<PromocionAplicada> it = turno.getPromociones().stream().distinct().iterator();
				while(it.hasNext()) {
					PromocionAplicada p = it.next();
					descripcion += "Tenes activada la promocion: " + p.getNombre() + " de ";
					if( p.getDescuento().floatValue() != 0 ) {
						descripcion += p.getDescuento().floatValue()+"% de descuento en: ";
						for(ServicioPromocionAplicada serv: p.getServicios()) {
							descripcion+= serv.getNombre() + "\n";
						}
								//+ "(Monto original: " +
								//((pago.getMontoTotal().floatValue() * 100 / (100  - p.getDescuento().floatValue())) + pago.getMontoAbonado().floatValue() )+ " )";
					}
					if(p.getMultiplicaPuntos().floatValue() != 0) {
						descripcion += " \nMultiplicas por " +p.getMultiplicaPuntos().intValue() +" los puntos"; 
					}
					
				}
				rep.setPromociones(descripcion);
			}
			else {
				rep.setPromociones("");
			}
			List<DetallePagoDTO> detallesDePagoDTO = new ArrayList<DetallePagoDTO>();
			
			for(DetallePago dp : pago.getDetallePago()) {
				DetallePagoDTO detallePagoDTO = new DetallePagoDTO();
				String monto = "";
				if(dp.getTipo().equals(TipoDetallePago.PUNTOS)) {
					monto = "" +dp.getMonto().intValue() +" pts";
				}
				else {
					monto = ""+ dp.getMonto().floatValue()+" $";
				}
				detallePagoDTO.setTipo(dp.getTipo().name());
				detallePagoDTO.setFecha(dp.getFecha().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
				detallePagoDTO.setMonto(monto);
				detallesDePagoDTO.add(detallePagoDTO);
			}
			//List<DetallePagoDTO> detallePagoDTO = detallePagoService.convertDetallePagoToDTOList(pago);
			rep.setDetallesDePago(detallesDePagoDTO);
		}
		catch(Exception e ) {

			e.printStackTrace();
		}
		System.out.println(rep);
		
		return rep;
	}

	@Override
	public JasperPrint exportPagoPdf(int idPago, int idTurno) {
		Pago pago = buscarPorId(idPago);
		Turno turno = turnoService.findById(idTurno);
		PagoReporte rep = convertToPagoReporte(pago, turno);
		
		Reporte reporte = new Reporte(rep);

		return reporte.mostrar();
	}

//	@Override
//	public void generarComprobante() {
//
//	}

}
