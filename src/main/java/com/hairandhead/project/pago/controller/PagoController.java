package com.hairandhead.project.pago.controller;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.service.ClienteService;
import com.hairandhead.project.pago.dto.DetallePagoDTO;
import com.hairandhead.project.pago.dto.PagoDTO;
import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.EstadoPago;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.pago.model.TipoDetallePago;
import com.hairandhead.project.pago.service.DetallePagoService;
import com.hairandhead.project.pago.service.PagoService;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.TurnoService;

import net.sf.jasperreports.engine.JasperExportManager;

@Controller
@RequestMapping("/pagos")
public class PagoController {
	@Autowired
	PagoService pagoService;
	
	@Autowired
	TurnoService turnoService;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	DetallePagoService detallePagoService;
	
	@RequestMapping(value = "/list" , method = RequestMethod.GET)
	public String listarPagos(ModelMap model) {
		model.addAttribute("pagos", pagoService.obtenerPagos());
		return "/pagos/listarPagos";
	}
	
	@RequestMapping(value = { "/create" }, method = RequestMethod.GET)
	public String create(@RequestParam int idTurno, ModelMap model)
	{
		PagoDTO pagoDTO = new PagoDTO();
		pagoDTO.setIdTurno(idTurno);
	
	
		Turno turno = turnoService.findById(idTurno);
		//Raro
		pagoDTO.setId(turno.getPago().getId());
		Cliente cliente = turno.getCliente();
		
		Pago pago = pagoService.buscarPorId(turno.getPago().getId());
		
		//Monto abonado no se actualiza nunca !! 
		pagoDTO.setMontoTotal(pago.getMontoTotal().subtract(pago.getMontoAbonado()).toString());
		
		List<DetallePagoDTO> detallesPago = detallePagoService.convertDetallePagoToDTOList(pago);
		pagoDTO.setDetallesDePago(detallesPago);
		
		try {
			pagoDTO.setPuntosAcumulados(Double.valueOf(cliente.getPuntosAcumulados()).intValue() );
		}
		catch(Exception e ) {
			e.printStackTrace();
			pagoDTO.setPuntosAcumulados(0);
		}
		//No queda bien aca.
		List<String> tiposDePago = new ArrayList<>();
		tiposDePago.add("EFECTIVO");
		tiposDePago.add("PUNTOS");
		
		DetallePagoDTO detallePagoDTO = new DetallePagoDTO();
		
		if(turno.getPromociones().size() != 0) {
			String descripcion = "";
			Iterator<PromocionAplicada> it = turno.getPromociones().stream().distinct().iterator();
			while(it.hasNext()) {
				PromocionAplicada p = it.next();
				descripcion += "Tenes activada la promocion: " + p.getNombre() + " de ";
				if( p.getDescuento().floatValue() != 0 ) {
					descripcion += p.getDescuento().floatValue()+"% de descuento en: ";
					for(ServicioPromocionAplicada serv: p.getServicios()) {
						descripcion+= serv.getNombre() + "\n";
					}
							//+ "(Monto original: " +
							//((pago.getMontoTotal().floatValue() * 100 / (100  - p.getDescuento().floatValue())) + pago.getMontoAbonado().floatValue() )+ " )";
				}
				if(p.getMultiplicaPuntos().floatValue() != 0) {
					descripcion += " \nMultiplicas por " +p.getMultiplicaPuntos().intValue() +" los puntos"; 
				}
				
			}
			model.addAttribute("promo", true);
			model.addAttribute("msgPromo", descripcion);
		}

		model.addAttribute("detallePagoDTO", detallePagoDTO);
		model.addAttribute("tiposDePago", tiposDePago);
		model.addAttribute("pagoDTO", pagoDTO);
		model.addAttribute("edit", false);
		return "/pagos/formPago";
	}

	
	@RequestMapping(value = { "/registrarDetallePago" }, method = RequestMethod.POST)
	public String createReserva(@Valid DetallePagoDTO detallePagoDTO, ModelMap model, BindingResult result) {
		
		if(result.hasErrors()) {
			for(ObjectError error: result.getAllErrors()) {
				System.out.println(error.getDefaultMessage());
			}
		}
		System.out.println("Detalle pago DTO:  " + detallePagoDTO);
		
		DetallePago detallePago = new DetallePago();
		detallePagoService.convertToDetallePago(detallePago, detallePagoDTO);
		
		Pago pago = pagoService.buscarPorId(Integer.valueOf(detallePagoDTO.getIdPago()));
		//No puede pagar con mas puntos de los que tiene
		Turno turno = turnoService.findById(detallePagoDTO.getIdTurno());
		
		if(detallePago.getTipo().equals(TipoDetallePago.PUNTOS)) {
			
			if (turno.getCliente().getPuntosAcumulados() >= detallePago.getMonto().doubleValue()) {
				pago.setMontoTotal(pago.getMontoTotal().subtract(detallePago.getMonto()));
			}
			else {
				System.out.println("Error 1");
				model.addAttribute("error", true);
				model.addAttribute("msgError", "No tenes los puntos ingresados suficientes para canjear");
				return "redirect:/pagos/create?idTurno="+detallePagoDTO.getIdTurno();
			}
		}
		else {
			if(detallePago.getMonto().floatValue() > pago.getMontoTotal().floatValue()) {
				model.addAttribute("error", true);
				model.addAttribute("msgError", "El monto ingresado es mayor al pago requerido");
				System.out.println("Error 2");
				return "redirect:/pagos/create?idTurno="+detallePagoDTO.getIdTurno();
			}
			else {
				pago.setMontoTotal(pago.getMontoTotal().subtract(detallePago.getMonto()));
			}
		}
		
		List<DetallePago> detallesDePago = pago.getDetallePago();
		detallesDePago.add(detallePago);
		pago.setDetallePago(detallesDePago);

		pagoService.editar(pago);
		
		//Tenemos que agregarle los puntos a la persona!, hacer esa cuenta aca abajo
		if(turno.getPromociones().size() != 0) {
			for(PromocionAplicada p : turno.getPromociones()) {
				if(p.getMultiplicaPuntos().floatValue() != 0) {
					turno.getCliente().setPuntosAcumulados(turno.getCliente().getPuntosAcumulados() + (detallePago.getMonto().floatValue() * p.getMultiplicaPuntos().floatValue() ));
				}
			}
			
		}
		else {
			turno.getCliente().setPuntosAcumulados(turno.getCliente().getPuntosAcumulados() + detallePago.getMonto().floatValue());
		}
		
		clienteService.updateCliente(turno.getCliente());
		
		return "redirect:/pagos/create?idTurno="+detallePagoDTO.getIdTurno();
	}

	@RequestMapping(value = {"/imprimir/{idTurno}/{idPago}"}, method = RequestMethod.GET)
	public void report(@PathVariable int idTurno , @PathVariable int idPago, HttpServletResponse response) throws Exception {
		response.setContentType("text/html");
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"comprobante.pdf\""));
		OutputStream out = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(pagoService.exportPagoPdf(idPago, idTurno ), out);
	}
}
