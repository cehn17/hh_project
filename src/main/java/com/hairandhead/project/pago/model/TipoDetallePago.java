package com.hairandhead.project.pago.model;

import java.io.Serializable;

public enum TipoDetallePago implements Serializable{
	EFECTIVO("EFECTIVO"),
	CANCELACION("CANCELACION"),
	PUNTOS("PUNTOS");
	
	String estadoPago;
	
	private TipoDetallePago(String userProfileType){
		this.estadoPago = userProfileType;
	}
	
}