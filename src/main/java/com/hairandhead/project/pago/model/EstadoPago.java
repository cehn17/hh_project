package com.hairandhead.project.pago.model;

import java.io.Serializable;

public enum EstadoPago implements Serializable{
	PENDIENTE("PENDIENTE"),
	PAGADO("PAGADO");
	
	String estadoPago;
	
	private EstadoPago(String userProfileType){
		this.estadoPago = userProfileType;
	}
	
}