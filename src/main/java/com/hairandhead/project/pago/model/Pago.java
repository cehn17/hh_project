package com.hairandhead.project.pago.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="Pago")
public class Pago implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private BigDecimal monto;
	@Column
	private BigDecimal montoTotal;
	@Column
	private BigDecimal montoAbonado;
	@Column
	private EstadoPago estado;
	@Column
	private LocalDate fecha;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<DetallePago> detallePago;

//	@OneToOne(mappedBy = "pago", cascade = CascadeType.ALL)
//	private Turno turno;
	
	public Pago() {
		this.estado = EstadoPago.PENDIENTE;
		this.fecha = null;
		this.detallePago = new ArrayList<DetallePago>();
		this.montoAbonado = new BigDecimal("0");
		this.montoTotal = new BigDecimal("0");
		this.monto = new BigDecimal("0");
		this.detallePago = new ArrayList<>();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public EstadoPago getEstado() {
		return estado;
	}
	public void setEstado(EstadoPago estado) {
		this.estado = estado;
	}
	public LocalDate getFecha() {
		return fecha;
	}
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	
	

//	public Turno getTurno() {
//		return turno;
//	}
//
//	public void setTurno(Turno turno) {
//		this.turno = turno;
//	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	public BigDecimal getMontoAbonado() {
		return montoAbonado;
	}
	public void setMontoAbonado(BigDecimal montoAbonado) {
		this.montoAbonado = montoAbonado;
	}
	public List<DetallePago> getDetallePago() {
		return detallePago;
	}
	public void setDetallePago(List<DetallePago> detallePago) {
		this.detallePago = detallePago;
	}
	

	@Override
	public String toString() {
		return "Pago [id=" + id + ", monto=" + monto + ", montoTotal=" + montoTotal + ", montoAbonado=" + montoAbonado
				+ ", estado=" + estado + ", fecha=" + fecha + ", detallePago=" + detallePago + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((detallePago == null) ? 0 : detallePago.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((montoAbonado == null) ? 0 : montoAbonado.hashCode());
		result = prime * result + ((montoTotal == null) ? 0 : montoTotal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pago other = (Pago) obj;
		if (detallePago == null) {
			if (other.detallePago != null)
				return false;
		} else if (!detallePago.equals(other.detallePago))
			return false;
		if (estado != other.estado)
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (montoAbonado == null) {
			if (other.montoAbonado != null)
				return false;
		} else if (!montoAbonado.equals(other.montoAbonado))
			return false;
		if (montoTotal == null) {
			if (other.montoTotal != null)
				return false;
		} else if (!montoTotal.equals(other.montoTotal))
			return false;
		return true;
	}
	
}
