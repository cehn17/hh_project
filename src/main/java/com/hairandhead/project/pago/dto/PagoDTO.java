package com.hairandhead.project.pago.dto;

import java.util.List;

public class PagoDTO {

	private Integer id;
	
	private Integer idTurno;
	
	private Integer puntosAcumulados;

	private String montoTotal;

	private String estado;

	private String fecha;
	
	private List<DetallePagoDTO> detallesDePago;
	
	public PagoDTO() {
		id = null;
		montoTotal = "";
		estado = "";
		fecha = "";
		idTurno =null;
		puntosAcumulados = null;
		detallesDePago = null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(String monto) {
		this.montoTotal = monto;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public Integer getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}
	
	public Integer getPuntosAcumulados() {
		return puntosAcumulados;
	}

	public void setPuntosAcumulados(Integer puntosAcumulados) {
		this.puntosAcumulados = puntosAcumulados;
	}
	
	
	public List<DetallePagoDTO> getDetallesDePago() {
		return detallesDePago;
	}

	public void setDetallesDePago(List<DetallePagoDTO> detallesDePago) {
		this.detallesDePago = detallesDePago;
	}

	@Override
	public String toString() {
		return "PagoDTO [id=" + id + ", montoTotal=" + montoTotal + ", estado=" + estado + ", fecha=" + fecha
				+ ", detallesDePago=" + detallesDePago + ", idTurno=" + idTurno + ", puntosAcumulados="
				+ puntosAcumulados + "]";
	}
	
	
	
	
}
