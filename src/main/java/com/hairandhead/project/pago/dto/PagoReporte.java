package com.hairandhead.project.pago.dto;

import java.util.List;
import java.util.Objects;

import com.hairandhead.project.promocion.dto.PromocionDTO;
import com.hairandhead.project.servicio.dto.ServicioDTO;

public class PagoReporte {
	
	private Integer id;
	
	private Integer idTurno;
	
	private Float monto;
	
	private Float montoTotal;
	
	private Float montoAbonado;

	private String estado;

	private String fecha;
	
	private List<DetallePagoDTO> detallesDePago;
	
	private String promociones;
	
	private List<ServicioDTO> servicios;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public List<DetallePagoDTO> getDetallesDePago() {
		return detallesDePago;
	}

	public void setDetallesDePago(List<DetallePagoDTO> detallesDePago) {
		this.detallesDePago = detallesDePago;
	}

	public List<ServicioDTO> getServicios() {
		return servicios;
	}

	public void setServicios(List<ServicioDTO> servicios) {
		this.servicios = servicios;
	}

	public String getPromociones() {
		return promociones;
	}

	public void setPromociones(String promociones) {
		this.promociones = promociones;
	}

	public Float getMonto() {
		return monto;
	}

	public void setMonto(Float monto) {
		this.monto = monto;
	}

	public Float getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(Float montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Float getMontoAbonado() {
		return montoAbonado;
	}

	public void setMontoAbonado(Float montoAbonado) {
		this.montoAbonado = montoAbonado;
	}

//	@Override
//	public String toString() {
//		return "PagoReporte [id=" + id + ", idTurno=" + idTurno + ", monto=" + monto + ", montoTotal=" + montoTotal
//				+ ", montoAbonado=" + montoAbonado + ", estado=" + estado + ", fecha=" + fecha + ", detallesDePago="
//				+ detallesDePago + ", promociones=" + promociones + ", servicios=" + servicios + "]";
//	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PagoReporte that = (PagoReporte) o;
		return Objects.equals(id, that.id) &&
				Objects.equals(idTurno, that.idTurno) &&
				Objects.equals(monto, that.monto) &&
				Objects.equals(montoTotal, that.montoTotal) &&
				Objects.equals(montoAbonado, that.montoAbonado) &&
				Objects.equals(estado, that.estado) &&
				Objects.equals(fecha, that.fecha) &&
				Objects.equals(detallesDePago, that.detallesDePago) &&
				Objects.equals(promociones, that.promociones) &&
				Objects.equals(servicios, that.servicios);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, idTurno, monto, montoTotal, montoAbonado, estado, fecha, detallesDePago, promociones, servicios);
	}
}
