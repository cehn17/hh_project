package com.hairandhead.project.pago.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.utils.AbstractDao;

@Repository("pagoDao")
public class PagoDaoImpl extends AbstractDao<Integer, Pago>implements PagoDao {

	@Override
	public Pago buscarPorId(int id) {
		return super.getByKey(id);
	}

	@Override
	public void guardar(Pago user) {
		super.persist(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pago> obtenerPagos() {
		Criteria crit = createEntityCriteria();
		//crit.addOrder(Order.asc("type"));
		return (List<Pago>)crit.list();
	}

	@Override
	public void editar(Pago p) {
		super.update(p);
	}
	
}
