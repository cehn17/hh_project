package com.hairandhead.project.pago.dao;

import java.util.List;

import com.hairandhead.project.pago.model.Pago;

public interface PagoDao {
	Pago buscarPorId(int id);
	
	void guardar(Pago user);
	
	List<Pago> obtenerPagos();
	
	void editar(Pago p);
	
	//Obtener pagos por sucursal (Sucursal s)
}
