package com.hairandhead.project.pago.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.utils.AbstractDao;
@Repository("detallePagoDao")
public class DetallePagoImpl extends AbstractDao<Integer, DetallePago> implements DetallePagoDao {

	@Override
	public DetallePago findById(int id) {
		return super.getByKey(id);
	}

	@Override
	public void save(DetallePago detallePago) {
		super.persist(detallePago);
	}

	@Override
	public void delete(DetallePago detallePago) {
		super.delete(detallePago);
	}

	@Override
	public void update(DetallePago detallePago) {
		super.update(detallePago);
	}

	
}
