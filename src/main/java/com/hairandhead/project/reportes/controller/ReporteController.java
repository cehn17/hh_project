package com.hairandhead.project.reportes.controller;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hairandhead.project.reportes.service.ReporteService;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.sucursal.service.SucursalService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
@Controller
@RequestMapping("/reportes")
public class ReporteController {
	
	@Autowired
	ReporteService reporteService;
	
	@Autowired
	SucursalService sucursalService;
	
	@RequestMapping(value = {"/list"}, method = RequestMethod.GET)
	public String list() {
		return "/reportes/listReportes";
	}
	
	@RequestMapping(value = {"/servicios"}, method = RequestMethod.GET)
	public void servicioMasRequerido(@RequestParam String inicio, @RequestParam String fin,@RequestParam String sucursal, HttpServletResponse response) throws IOException, JRException {
		Sucursal suc = sucursalService.buscarPorId(Integer.valueOf(sucursal));
		System.out.println("Sucursal: " + suc.getNombre());
		response.setContentType("text/html");
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"comprobante.pdf\""));
		OutputStream out = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(reporteService.servicios(suc.getNombre(), inicio, fin), out);
	}
	
	@RequestMapping(value = {"/profesionales"}, method = RequestMethod.GET)
	public void recaudacionPorProfesional(@RequestParam String inicio, @RequestParam String fin, @RequestParam String sucursal, HttpServletResponse response) throws IOException, JRException {
		Sucursal suc = sucursalService.buscarPorId(Integer.valueOf(sucursal));
		response.setContentType("text/html");
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"comprobante.pdf\""));
		OutputStream out = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(reporteService.profesionales(suc.getNombre(), inicio, fin), out);
	}
	
	@RequestMapping(value = {"/servRecaudacion"}, method = RequestMethod.GET)
	public void recaudacionPorServicio(@RequestParam String inicio, @RequestParam String fin, @RequestParam String sucursal, HttpServletResponse response) throws IOException, JRException {
		System.out.println("Entre al reporte");
		Sucursal suc = sucursalService.buscarPorId(Integer.valueOf(sucursal));
		response.setContentType("text/html");
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"comprobante.pdf\""));
		OutputStream out = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(reporteService.recaudacionPorServicio(suc.getNombre(), inicio, fin), out);
	}
	
}
