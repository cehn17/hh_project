package com.hairandhead.project.reportes.dto;

public class ProfesionalReporte {
	private String nombre;
	private Double ganancia;
	private String gananciaTotal;
	
	public ProfesionalReporte() {
		nombre = "";
		ganancia = 0.0;
		gananciaTotal = "";
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Double getGanancia() {
		return ganancia;
	}
	public void setGanancia(Double ganancia) {
		this.ganancia = ganancia;
	}
	public String getGananciaTotal() {
		return gananciaTotal;
	}
	public void setGananciaTotal(String gananciaTotal) {
		this.gananciaTotal = gananciaTotal;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == null) return false;
		if(!(o instanceof ProfesionalReporte)) return false;
		ProfesionalReporte other = (ProfesionalReporte) o;
		return this.getNombre().equals(other.getNombre());
	}
	
	
	
}
