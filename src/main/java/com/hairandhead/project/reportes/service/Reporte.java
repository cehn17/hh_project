package com.hairandhead.project.reportes.service;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.log4j.Logger;

import com.hairandhead.project.pago.dto.PagoDTO;
import com.hairandhead.project.pago.dto.PagoReporte;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.reportes.dto.ProfesionalReporte;
import com.hairandhead.project.reportes.dto.ServicioReporte;
import com.hairandhead.project.servicio.dto.ServicioDTO;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


public class Reporte {
private JasperReport reporte;
private JasperPrint	reporteLleno;
private Logger log = Logger.getLogger(Reporte.class);
    public Reporte(PagoReporte pago)
    {
		Map<String, Object> parametersMap = new HashMap<String, Object>();
		parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		ArrayList<PagoReporte> pagos = new ArrayList<>();
		pagos.add(pago);
		JRBeanCollectionDataSource servicios = new JRBeanCollectionDataSource(pago.getServicios());
		JRBeanCollectionDataSource detallesDePago = new JRBeanCollectionDataSource(pago.getDetallesDePago());
		parametersMap.put("Servicios", servicios);
		parametersMap.put("DetallesDePago", detallesDePago);

    	try	{

    		Resource subReporteServicio = new ClassPathResource("reportes/servicios.jasper");
    		Resource subReporteDetallePago = new ClassPathResource("reportes/detallesDePago.jasper");

    		parametersMap.put("subReport", subReporteServicio.getFile().getAbsolutePath());
    		parametersMap.put("subReportDetallesDePago", subReporteDetallePago.getFile().getAbsolutePath());

    		Resource urlLogo = new ClassPathResource("reportes/hyh.png");
			String url = urlLogo.getFile().getAbsolutePath();
			parametersMap.put("UrlLogo", url);

			Resource resource = new ClassPathResource("reportes/comprobantepago.jrxml");
			System.out.println(resource.getFile().getAbsolutePath());
			this.reporte = JasperCompileManager.compileReport(resource.getFile().getAbsolutePath());

			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap,
					new JRBeanCollectionDataSource(pagos));

    		log.info("Se cargó correctamente el reporte");
    		System.out.println("Termine el reporte");
		}
		catch( Exception e )
		{	e.printStackTrace();
			log.error("Ocurrió un error mientras se cargaba el archivo ReporteAgenda.Jasper");
		}
    }

    public Reporte(HashMap<String, Integer> serviciosMasConsumidos) {

    }
    
    public Reporte(List<ServicioDTO> serviciosMasConsumidos, String inicio, String fin , String sucursal) {
    	Map<String, Object> parametersMap = new HashMap<String, Object>();
    	parametersMap.put("Inicio", inicio);
    	parametersMap.put("Fin", fin);
    	parametersMap.put("Sucursal", sucursal);
    	serviciosMasConsumidos.sort((e1, e2) -> e1.getNombre().compareTo(e2.getNombre()));
    	try		{
    		
			Resource urlLogo = new ClassPathResource("reportes/hyh.png");
			String url = urlLogo.getFile().getAbsolutePath();
			parametersMap.put("UrlLogo", url);

			Resource resource = new ClassPathResource("reportes/rankingservicios.jrxml");
			this.reporte = JasperCompileManager.compileReport(resource.getFile().getAbsolutePath());

			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap,
					new JRBeanCollectionDataSource(serviciosMasConsumidos));

    		log.info("Se cargó correctamente el reporte");
    		System.out.println("Termine el reporte");
		}
		catch( Exception e )
		{	e.printStackTrace();
			log.error("Ocurrió un error mientras se cargaba el archivo ReporteAgenda.Jasper");
		}
    	
    }

    public Reporte(List<ProfesionalReporte> profesionalReporte, String inicio, String fin, Double recaudacionReal, Double total, String sucursal) {
    	Map<String, Object> parametersMap = new HashMap<String, Object>();
    	parametersMap.put("Inicio", inicio);
    	parametersMap.put("Fin", fin);
    	parametersMap.put("RecaudacionReal", recaudacionReal);
    	parametersMap.put("Total", total);
    	parametersMap.put("Sucursal", sucursal);
    	try		{
			Resource resource = new ClassPathResource("reportes/profesionalreporte.jrxml");
			Resource urlLogo = new ClassPathResource("reportes/hyh.png");
			String url = urlLogo.getFile().getAbsolutePath();
			parametersMap.put("UrlLogo", url);
			
			this.reporte = JasperCompileManager.compileReport(resource.getFile().getAbsolutePath());

			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap,
					new JRBeanCollectionDataSource(profesionalReporte));
    		log.info("Se cargó correctamente el reporte");
		}
		catch( Exception e )
		{	e.printStackTrace();
			log.error("Ocurrió un error mientras se cargaba el archivo ReporteAgenda.Jasper");
		}
	}
    
    public Reporte(List<ServicioReporte> servicioReporte, String inicio, String fin, Double total, String sucursal){
    	System.out.println("rep");
    	Map<String, Object> parametersMap = new HashMap<String, Object>();
    	parametersMap.put("Inicio", inicio);
    	parametersMap.put("Fin", fin);
    	parametersMap.put("Total", total);
    	parametersMap.put("Sucursal", sucursal);
    	try		{
			Resource resource = new ClassPathResource("reportes/servrecaudacionreporte.jrxml");
			Resource urlLogo = new ClassPathResource("reportes/hyh.png");
			String url = urlLogo.getFile().getAbsolutePath();
			parametersMap.put("UrlLogo", url);
			
			this.reporte = JasperCompileManager.compileReport(resource.getFile().getAbsolutePath());

			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap,
					new JRBeanCollectionDataSource(servicioReporte));
    		
		}
		catch( Exception e )
		{	e.printStackTrace();
			log.error("Ocurrió un error mientras se cargaba el archivo ReporteAgenda.Jasper");
		}
    }

	public JasperPrint mostrar()
	{	
		return this.reporteLleno;
	}
   

}
