package com.hairandhead.project.reportes.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.pago.model.TipoDetallePago;
import com.hairandhead.project.reportes.dto.ProfesionalReporte;
import com.hairandhead.project.reportes.dto.ServicioReporte;
import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.TurnoService;

import net.sf.jasperreports.engine.JasperPrint;

@Service("reporteService")
@Transactional
public class ReporteServiceImpl implements ReporteService{

	@Autowired
	TurnoService turnoService;
	@Autowired
	ServicioService servicioService;
	
	
	@Override
	public JasperPrint servicios(String nombreSucursal, String inicio, String fin) {
		LocalDate in= LocalDate.parse(inicio, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		LocalDate fn= LocalDate.parse(fin, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		List<Turno> turnos = turnoService.findAllBetween(in, fn);
		List<ServicioDTO> serviciosMasConsumidos = serviciosMasConsumidos(turnos);
		Reporte reporte = new Reporte(serviciosMasConsumidos, inicio, fin, nombreSucursal);
		
		return reporte.mostrar();
	}
	
//	public HashMap<String, Integer> serviciosRanking(List<Turno> turnos){
//		HashMap<String, Integer> ret = new HashMap<String, Integer>();
//
//		for(Turno turno : turnos) {
//			for(DetalleTurno dt : turno.getDetalleTurnos()) {
//				String nombre = dt.getServicio().getNombre();
//				if(ret.containsKey(nombre)){
//					ret.put(nombre, ret.get(nombre) + 1);
//				}
//				else {
//					ret.put(nombre, 1);
//				}
//			}
//		}
//
//		return ret;
//	}
	
	public List<ServicioDTO> serviciosMasConsumidos(List<Turno> turnos){
		ArrayList<ServicioDTO> ret = new ArrayList<ServicioDTO>();

		for(Turno turno : turnos) {
			for(DetalleTurno dt : turno.getDetalleTurnos()) {
				ServicioDTO sv = servicioService.convertToDTO(dt.getServicio());
			servicioService.convertToDTO(dt.getServicio(), sv);
				ret.add(sv);
			}
		}

		return ret;

	}

	@Override
	public JasperPrint profesionales(String nombreSucursal,String inicio, String fin) {
		LocalDate in= LocalDate.parse(inicio, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		LocalDate fn= LocalDate.parse(fin, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		
		List<Turno> turnos = turnoService.findAllBetween(in, fn);

		List<ProfesionalReporte> profesionalReporte = gananciaPorProfesional(turnos);
		
		Double recaudacionReal = recaudacionReal(turnos);
		
		Double total = ingresoTotal(profesionalReporte);
		
		
		Reporte reporte = new Reporte(profesionalReporte, inicio, fin, recaudacionReal, total, nombreSucursal);
		
		return reporte.mostrar();
	
	}
	
	public List<ProfesionalReporte> gananciaPorProfesional(List<Turno> turnos) {
		List<ProfesionalReporte> ret = new ArrayList<ProfesionalReporte>();
		
		HashMap<String, Double> profesionalXRecaudacion = new HashMap<>();
		
		for(Turno turno: turnos) {
				
			for(DetalleTurno dt: turno.getDetalleTurnos()) {
				String profesional = dt.getProfesional().getNombre()+ " "+ dt.getProfesional().getApellido();	
				if(!(profesionalXRecaudacion.containsKey(profesional))) {
					profesionalXRecaudacion.put(profesional,dt.getServicio().getPrecio().doubleValue());
				}
				else {
					profesionalXRecaudacion.put(profesional,
							profesionalXRecaudacion.get(profesional) + 
							dt.getServicio().getPrecio().doubleValue());
				}
			}
			
		}
		
		for(String profesional : profesionalXRecaudacion.keySet()) {
			ProfesionalReporte profesionalReporte = new ProfesionalReporte();
			profesionalReporte.setNombre(profesional);
			profesionalReporte.setGanancia(profesionalXRecaudacion.get(profesional));
			profesionalReporte.setGananciaTotal(profesionalXRecaudacion.get(profesional).toString());
			ret.add(profesionalReporte);
		}
		
		return ret;
	}
	
	public Double recaudacionReal(List<Turno> turnos) {
		Double ret = 0.0;
		for(Turno turno : turnos) {
			if(turno.getPago()!= null) {
				for(DetallePago detallePago : turno.getPago().getDetallePago()) {
					if(detallePago.getTipo().equals(TipoDetallePago.EFECTIVO)) {
						ret = ret + detallePago.getMonto().doubleValue();
					}
				}
			}
		}
		
		return ret;
	}
	
	public Double ingresoTotal(List<ProfesionalReporte> pr) {
		Double ret = 0.0;
		for(ProfesionalReporte prof: pr) {
			ret += prof.getGanancia();
		}
		return ret;
	}
	
	public Double ingresoTotalServ(List<ServicioReporte> pr) {
		Double ret = 0.0;
		for(ServicioReporte prof: pr) {
			ret += prof.getGanancia();
		}
		return ret;
	}

	@Override
	public JasperPrint recaudacionPorServicio(String nombreSucursal, String inicio, String fin) {
		LocalDate in= LocalDate.parse(inicio, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		LocalDate fn= LocalDate.parse(fin, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		
		List<Turno> turnos = turnoService.findAllBetween(in, fn);
		
		List<ServicioReporte> servicioReporte = gananciaPorServicio(turnos);
		
		System.out.println(servicioReporte);
		
		Double total = ingresoTotalServ(servicioReporte);
		
		System.out.println(total);
		
		Reporte reporte = new Reporte(servicioReporte, inicio, fin, total, nombreSucursal);
		
		return reporte.mostrar();
	}

	private List<ServicioReporte> gananciaPorServicio(List<Turno> turnos) {
		List<ServicioReporte> ret = new ArrayList<ServicioReporte>();
		
		HashMap<String, Double> servicioXRecaudacion = new HashMap<>();
		
		for(Turno turno: turnos) {
				
			for(DetalleTurno dt: turno.getDetalleTurnos()) {
				String servicio = dt.getServicio().getNombre();	
				if(!(servicioXRecaudacion.containsKey(servicio))) {
					servicioXRecaudacion.put(servicio,dt.getServicio().getPrecio().doubleValue());
				}
				else {
					servicioXRecaudacion.put(servicio,
							servicioXRecaudacion.get(servicio) + 
							dt.getServicio().getPrecio().doubleValue());
				}
			}
			
		}
		
		for(String serv : servicioXRecaudacion.keySet()) {
			ServicioReporte servicioReporte = new ServicioReporte();
			servicioReporte.setNombre(serv);
			servicioReporte.setGanancia(servicioXRecaudacion.get(serv));
			ret.add(servicioReporte);
		}
		
		return ret;
	}


}
