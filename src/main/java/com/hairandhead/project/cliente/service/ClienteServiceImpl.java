package com.hairandhead.project.cliente.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hairandhead.project.cliente.dao.ClienteDao;
import com.hairandhead.project.cliente.model.Cliente;

@Service("ClienteService")
@Transactional
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteDao dao;
	
	@Override
	public Cliente findClienteById(int id) {
		return dao.findById(id);
	}
	@Override
	public Cliente findClienteByMail(String mail) {
		return dao.findByMail(mail);
	}

	@Override
	public void saveCliente(Cliente c) {
		dao.save(c);
	}

	@Override
	public void updateCliente(Cliente c) {
		dao.update(c);
	}

	@Override
	public List<Cliente> findAllClientes() {
		return dao.findAll();
	}

	@Override
	public List<Cliente> findAllClientesActivos() {
		// TODO Auto-generated method stub
		return dao.findAllClientesActivos();
	}

}
