package com.hairandhead.project.promocion.model;

import com.hairandhead.project.servicio.model.EstadoServicio;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@Table(name="ServiciosPromocionAplicada")
public class ServicioPromocionAplicada {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name="Nombre", nullable=false)
    private String nombre;


    @Column(name="Descripcion", nullable=false)
    private String descripcion;


    @Digits(integer=8, fraction=2)
    @Column(name = "Precio")
    private BigDecimal precio;

    @Column(name = "TiempoPromedioDeDuracion")
    private Duration tiempoPromedioDeDuracion;

    @Column(name="Estado")
    private EstadoServicio estado;

    @Column(name="FechaDeBaja")
    private LocalDateTime fechaDeBaja;

    public ServicioPromocionAplicada(){
        this.id = null;
        this.nombre = "";
        this.descripcion = "";
        this.precio = new BigDecimal("0");
        this.tiempoPromedioDeDuracion = null;
        this.estado = EstadoServicio.ACTIVO;
        this.fechaDeBaja = null;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Duration getTiempoPromedioDeDuracion() {
        return tiempoPromedioDeDuracion;
    }

    public void setTiempoPromedioDeDuracion(Duration tiempoPromedioDeDuracion) {
        this.tiempoPromedioDeDuracion = tiempoPromedioDeDuracion;
    }

    public EstadoServicio getEstado() {
        return estado;
    }

    public void setEstado(EstadoServicio estado) {
        this.estado = estado;
    }

    public LocalDateTime getFechaDeBaja() {
        return fechaDeBaja;
    }

    public void setFechaDeBaja(LocalDateTime fechaDeBaja) {
        this.fechaDeBaja = fechaDeBaja;
    }
}
