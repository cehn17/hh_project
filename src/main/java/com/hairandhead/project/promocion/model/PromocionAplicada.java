package com.hairandhead.project.promocion.model;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name ="Promociones_Aplicadas")
public class PromocionAplicada {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="Nombre")
    private String nombre;

    @Digits(integer=8, fraction=2)
    @Column(name="Descuento")
    private BigDecimal descuento;

    @Column(name="FechaDeAlta")
    private LocalDate fechaDeAlta;

    @Column(name="FechaDeVencimiento")
    private LocalDate fechaDeVencimiento;

    @Column(name="FechaDeBaja")
    private LocalDate fechaDeBaja;

    @Column(name="Estado")
    private EstadoPromocion estado;

    @Column(name="ValorFinal")
    private BigDecimal valorFinal;

    @Column(name="MultiplicaPuntos")
    private BigDecimal multiplicaPuntos;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private List<ServicioPromocionAplicada> servicios;

    public PromocionAplicada() {
        this.estado = EstadoPromocion.ACTIVO;
        this.fechaDeAlta = null;
        this.fechaDeVencimiento = null;
        this.fechaDeBaja = null;
        this.servicios = new ArrayList<>();
        this.multiplicaPuntos = new BigDecimal("1");
        this.descuento = new BigDecimal("0");
        this.valorFinal = new BigDecimal("0");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getDescuento() {
        return descuento;
    }

    public void setDescuento(BigDecimal descuento) {
        this.descuento = descuento;
    }

    public LocalDate getFechaDeAlta() {
        return fechaDeAlta;
    }

    public void setFechaDeAlta(LocalDate fechaDeAlta) {
        this.fechaDeAlta = fechaDeAlta;
    }

    public LocalDate getFechaDeVencimiento() {
        return fechaDeVencimiento;
    }

    public void setFechaDeVencimiento(LocalDate fechaDeVencimiento) {
        this.fechaDeVencimiento = fechaDeVencimiento;
    }

    public LocalDate getFechaDeBaja() {
        return fechaDeBaja;
    }

    public void setFechaDeBaja(LocalDate fechaDeBaja) {
        this.fechaDeBaja = fechaDeBaja;
    }

    public EstadoPromocion getEstado() {
        return estado;
    }

    public void setEstado(EstadoPromocion estado) {
        this.estado = estado;
    }

    public List<ServicioPromocionAplicada> getServicios() {
        return servicios;
    }

    public void setServicios(List<ServicioPromocionAplicada> servicios) {
        this.servicios = servicios;
    }

    public BigDecimal getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(BigDecimal valorFinal) {
        this.valorFinal = valorFinal;
    }

    public BigDecimal getMultiplicaPuntos() {
        return multiplicaPuntos;
    }

    public void setMultiplicaPuntos(BigDecimal multiplicaPuntos) {
        this.multiplicaPuntos = multiplicaPuntos;
    }
    
}
