package com.hairandhead.project.promocion.service;

import com.hairandhead.project.promocion.dao.PromocionAplicadaDao;
import com.hairandhead.project.promocion.dto.PromocionAplicadaDTO;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service("PromocionAplicadaService")
public class PromocionAplicadaServiceImpl implements PromocionAplicadaService {

    @Autowired
    private PromocionAplicadaDao promocionAplicadaDao;

    @Override
    public PromocionAplicada findPromocionAplicadaById(int id) {
        return promocionAplicadaDao.findById(id);
    }

    @Override
    public void savePromocionAplicada(PromocionAplicada p) {
        promocionAplicadaDao.save(p);
    }

    @Override
    public void updatePromocion(PromocionAplicada p) {
        promocionAplicadaDao.update(p);
    }

    @Override
    public List<PromocionAplicada> findAllPromocionesAplicadas() {
        return promocionAplicadaDao.findAll();
    }
    @Override
    public PromocionAplicadaDTO convertPromocionApliocadaToDTO(PromocionAplicada p) {
        if(p == null){
            return null;
        }

        DecimalFormat df = new DecimalFormat();
        PromocionAplicadaDTO dto = new PromocionAplicadaDTO();
        dto.setId(p.getId());
        dto.setNombre(p.getNombre());
        dto.setFechaDeAlta(p.getFechaDeAlta()!=null?p.getFechaDeAlta().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")):null);
        dto.setFechaDeVencimiento(p.getFechaDeVencimiento()!=null?p.getFechaDeVencimiento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")):null);
        dto.setFechaDeBaja(p.getFechaDeBaja()!=null?p.getFechaDeBaja().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")):null);
        dto.setDescuento(p.getDescuento()!=null?df.format(p.getDescuento()):"");
        dto.setMultiplicaPuntos(p.getMultiplicaPuntos()!=null?df.format(p.getMultiplicaPuntos()):"");
        dto.setEstado(p.getEstado().name());
        dto.setValorFinal(p.getValorFinal()!=null?df.format(p.getValorFinal()):"");
        return dto;
    }

    @Override
    public List<PromocionAplicadaDTO> convertPromocionAplicadaToDTOList(List<PromocionAplicada> promocionList) {
        List<PromocionAplicadaDTO> res = new ArrayList<>();
        promocionList.forEach(promo -> res.add(convertPromocionApliocadaToDTO(promo)));
        return res;
    }
}
