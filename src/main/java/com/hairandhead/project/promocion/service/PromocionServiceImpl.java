package com.hairandhead.project.promocion.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.promocion.dto.PromocionAplicadaDTO;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hairandhead.project.promocion.dao.PromocionDao;
import com.hairandhead.project.promocion.dto.PromocionDTO;
import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;

@Transactional
@Service("PromocionService")
public class PromocionServiceImpl implements PromocionService {

	@Autowired
	PromocionDao dao;
	
	@Override
	public Promocion findPromocionById(int id) {
		return dao.findById(id);
	}

	@Override
	public void savePromocion(Promocion p) {
		dao.save(p);
	}

	@Override
	public void updatePromocion(Promocion p) {
		dao.update(p);
	}

	@Override
	public List<Promocion> findAllPromociones() {
		return dao.findAll();
	}

	@Override
	public Promocion convertToPromocion(PromocionDTO dto) {
		if(dto == null) return null;
		
		Promocion p = new Promocion();
		p.setId(dto.getId());
		p.setNombre(dto.getNombre()!=null?dto.getNombre():null);
		p.setDescuento(new BigDecimal(dto.getDescuento()));
		p.setFechaDeAlta((dto.getFechaDeAlta()!=null && dto.getFechaDeAlta()!="")?LocalDate.parse(dto.getFechaDeAlta(), DateTimeFormatter.ofPattern("dd/MM/yyyy")):null);
		p.setFechaDeVencimiento((dto.getFechaDeVencimiento()!=null && dto.getFechaDeVencimiento()!="")?LocalDate.parse(dto.getFechaDeVencimiento(), DateTimeFormatter.ofPattern("dd/MM/yyyy")):null); 
		p.setFechaDeBaja((dto.getFechaDeBaja()!=null && dto.getFechaDeBaja()!="")?LocalDate.parse(dto.getFechaDeBaja(), DateTimeFormatter.ofPattern("dd/MM/yyyy")):null);
		p.setEstado(dto.getEstado().equals("ACTIVO")?EstadoPromocion.ACTIVO:EstadoPromocion.INACTIVO);
		p.setMultiplicaPuntos(new BigDecimal(dto.getMultiplicaPuntos()));
//		p.setValorFinal(new BigDecimal(dto.getValorFinal()));
//		p.setServicios(dto.getServicios());
		return p;
	}

	@Override
	public PromocionDTO convertToDTO(Promocion p) {
		if(p == null) return null;
		
		DecimalFormat df = new DecimalFormat();
		PromocionDTO dto = new PromocionDTO();
		dto.setId(p.getId());
		dto.setNombre(p.getNombre());
		dto.setFechaDeAlta(p.getFechaDeAlta()!=null?p.getFechaDeAlta().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")):null);
		dto.setFechaDeVencimiento(p.getFechaDeVencimiento()!=null?p.getFechaDeVencimiento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")):null);
		dto.setFechaDeBaja(p.getFechaDeBaja()!=null?p.getFechaDeBaja().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")):null);
		dto.setDescuento(p.getDescuento()!=null?df.format(p.getDescuento()):"");
		dto.setMultiplicaPuntos(p.getMultiplicaPuntos()!=null?df.format(p.getMultiplicaPuntos()):"");
		dto.setEstado(p.getEstado().name());
		dto.setValorFinal(p.getValorFinal()!=null?df.format(p.getValorFinal()):"");
		return dto;
	}

	@Override
	public List<PromocionDTO> convertToDTOList(List<Promocion> promocionList) {
		List<PromocionDTO> res = new ArrayList<>();
		promocionList.forEach(promo -> res.add(convertToDTO(promo)));
		return res;
	}

	@Override
	public List<Promocion> findByEstado(EstadoPromocion estadoPromocion) {
		return dao.findByEstado(estadoPromocion);
	}

	@Override
	public PromocionAplicada applyPromotion(Promocion promo) {
		PromocionAplicada res = new PromocionAplicada();
		List<ServicioPromocionAplicada> servicioPromocionAplicadaList = new ArrayList<>();
		res.setNombre(promo.getNombre());
		res.setDescuento(promo.getDescuento());
		res.setFechaDeAlta(promo.getFechaDeAlta());
		res.setFechaDeVencimiento(promo.getFechaDeVencimiento());
		res.setFechaDeBaja(promo.getFechaDeBaja());
		res.setEstado(promo.getEstado());
		res.setValorFinal(promo.getValorFinal());
		res.setMultiplicaPuntos(promo.getMultiplicaPuntos());
		promo.getServicios().forEach(servicio -> {
			ServicioPromocionAplicada servicioPromocionAplicada = new ServicioPromocionAplicada();
			servicioPromocionAplicada.setNombre(servicio.getNombre());
			servicioPromocionAplicada.setDescripcion(servicio.getDescripcion());
			servicioPromocionAplicada.setPrecio(servicio.getPrecio());
			servicioPromocionAplicada.setEstado(servicio.getEstado());
			servicioPromocionAplicada.setFechaDeBaja(servicio.getFechaDeBaja());
			servicioPromocionAplicadaList.add(servicioPromocionAplicada);
		});
		res.setServicios(servicioPromocionAplicadaList);
		return res;
	}
}
