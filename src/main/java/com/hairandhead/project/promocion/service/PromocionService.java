package com.hairandhead.project.promocion.service;

import java.util.List;

import com.hairandhead.project.promocion.dto.PromocionAplicadaDTO;
import com.hairandhead.project.promocion.dto.PromocionDTO;
import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;

public interface PromocionService {

	Promocion findPromocionById(int id);
	void savePromocion(Promocion p);
	void updatePromocion(Promocion p);
	List<Promocion> findAllPromociones();
	Promocion convertToPromocion(PromocionDTO dto);
	PromocionDTO convertToDTO(Promocion p);
	List<PromocionDTO> convertToDTOList(List<Promocion> promocionList);
	List<Promocion> findByEstado(EstadoPromocion estadoPromocion);
	PromocionAplicada applyPromotion(Promocion promo);

}
