package com.hairandhead.project.promocion.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.utils.AbstractDao;

@Repository("PromocionDao")
public class PromocionDaoImpl extends AbstractDao<Integer, Promocion> implements PromocionDao{

	@Override
	public Promocion findById(int id) {
		return super.getByKey(id);
	}

	@Override
	public void save(Promocion p) {
		super.persist(p);
	}
	
	@Override
	public void update(Promocion p)
	{
		super.update(p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Promocion> findAll() {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("id"));
		return (List<Promocion>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Promocion> findByEstado(EstadoPromocion estadoPromocion) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("estado", estadoPromocion));
		return (List<Promocion>) criteria.list();
	}

}
