package com.hairandhead.project.promocion.dao;

import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;

import java.util.List;

public interface PromocionAplicadaDao {
    PromocionAplicada findById(int id);

    void save(PromocionAplicada p);

    void update(PromocionAplicada p);

    List<PromocionAplicada> findAll();
}
