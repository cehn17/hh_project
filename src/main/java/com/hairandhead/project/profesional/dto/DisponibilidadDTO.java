package com.hairandhead.project.profesional.dto;

import java.util.ArrayList;
import java.util.List;

public class DisponibilidadDTO {
	
	  private Integer id;

	  private String dia;
	  private List<HorarioDTO> horaDisponible;
	    
	    public DisponibilidadDTO() {
	        this.id = null;
	        this.dia = "";
	        this.horaDisponible =  new ArrayList<HorarioDTO> ();
	    }
	    public DisponibilidadDTO(String dia) {
	        this.id = null;
	        this.dia = dia;
	        this.horaDisponible =  new ArrayList<HorarioDTO> ();

	    }

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getDia() {
			return dia;
		}

		public void setDia(String dia) {
			this.dia = dia;
		}
		public List<HorarioDTO> getHoraDisponible() {
			return horaDisponible;
		}
		public void setHoraDisponible(List<HorarioDTO> horaDisponible) {
			this.horaDisponible = horaDisponible;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((dia == null) ? 0 : dia.hashCode());
			result = prime * result + ((horaDisponible == null) ? 0 : horaDisponible.hashCode());
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DisponibilidadDTO other = (DisponibilidadDTO) obj;
			if (dia == null) {
				if (other.dia != null)
					return false;
			} else if (!dia.equals(other.dia))
				return false;
			if (horaDisponible == null) {
				if (other.horaDisponible != null)
					return false;
			} else if (!horaDisponible.equals(other.horaDisponible))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}
		@Override
		public String toString() {
			return "DisponibilidadDTO [id=" + id + ", dia=" + dia + ", horaDisponible=" + horaDisponible + "]";
		}
	
}
