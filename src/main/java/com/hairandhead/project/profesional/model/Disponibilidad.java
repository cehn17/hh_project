package com.hairandhead.project.profesional.model;

import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "disponibilidad")
public class Disponibilidad implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String dia;

    @ManyToMany(cascade = CascadeType.MERGE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Horario> horaDisponible;


    public Disponibilidad() {
        this.dia = "";
        this.horaDisponible = new ArrayList<Horario>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public List<Horario> getHoraDisponible() {
		return horaDisponible;
	}

	public void setHoraDisponible(List<Horario> horaDisponible) {
		this.horaDisponible = horaDisponible;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dia == null) ? 0 : dia.hashCode());
		result = prime * result + ((horaDisponible == null) ? 0 : horaDisponible.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Disponibilidad other = (Disponibilidad) obj;
		if (dia == null) {
			if (other.dia != null)
				return false;
		} else if (!dia.equals(other.dia))
			return false;
		if (horaDisponible == null) {
			if (other.horaDisponible != null)
				return false;
		} else if (!horaDisponible.equals(other.horaDisponible))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Disponibilidad [id=" + id + ", dia=" + dia + ", horaDisponible=" + horaDisponible + "]";
	}
	
	
    
}
