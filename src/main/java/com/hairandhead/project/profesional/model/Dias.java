package com.hairandhead.project.profesional.model;

public enum Dias {
	LUNES(0),
	MARTES(1),
	MIERCOLES(2),
	JUEVES(3),
	VIERNES(4),
	SABADO(5),
	DOMINGO(6);
	
	private final int code;

	Dias(int code) {
        this.code = code;
    }
    
    public int getCode() {
        return this.code;
    }
}
