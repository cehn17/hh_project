package com.hairandhead.project.profesional.dao;

import com.hairandhead.project.profesional.dao.ProfesionalDao;
import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.utils.AbstractDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("DisponibiliadDao")
public class DisponibilidadDaoImpl extends AbstractDao<Integer, Disponibilidad> implements DisponibilidadDao {
    @Override
    public Disponibilidad findById(int id) {
        return super.getByKey(id);
    }

    @Override
    public void save(Disponibilidad disponibilidad) {
        super.persist(disponibilidad);
    }
    
    @Override
    public void update(Disponibilidad disponibilidad) {
    	super.update(disponibilidad);
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<Disponibilidad> findAll() {
        Criteria criteria = createEntityCriteria().addOrder(Order.asc("id"));
		List<Disponibilidad> disponibilidades = (List<Disponibilidad>) criteria.list();
		
        return disponibilidades;
    }
}
