:: make sure to change the settings from line 4-9
set dbUser=root
set dbPassword=root
set backupDir="C:\backup"
set mysql=%1


:: create backup folder if it doesn't exist
if not exist %backupDir%  mkdir %backupDir%
:: iterate over the folder structure in the "data" folder to get the databases
	:: remove echo here if you like
	echo processing folder "%%f"

	::mysqldump -uroot -proot --default-character-set=utf8mb4 hh-project --result-file="C:/backup/11_Dec_2019/hh-project.sql"
	::mysql -hlocalhost -uroot -proot
	::create database hh_project;
	::use hh_project;
	%mysql% -u%dbUser% -p%dbPassword% < %backupDir%/hh_project.sql

popd