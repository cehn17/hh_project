:: make sure to change the settings from line 4-9
@echo off  
set dbUser=root
set dbPassword=root
set backupDir="C:\backup"
set mysqldump=%1

:: create backup folder if it doesn't exist
if not exist %backupDir%   mkdir %backupDir%

:: iterate over the folder structure in the "data" folder to get the databases
	:: remove echo here if you like
	echo processing folder "%%f"

	%mysqldump% --host="localhost" --user=%dbUser% --password=%dbPassword% --single-transaction --add-drop-table --databases hh_project> %backupDir%/hh_project.sql

popd