package com.hairandhead.project.promocion.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.promocion.dto.PromocionDTO;
import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.service.PromocionService;
import com.hairandhead.project.promocion.utils.Promociones;
import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioServiceImpl;
import com.hairandhead.project.servicio.utils.Servicios;

public class PromocionControllerTest {

	@Mock
    private PromocionService promocionService;
     
//    @Mock
//    MessageSource message;
	
	@Mock
	private ServicioServiceImpl servicioService;
     
    @InjectMocks
    private PromocionController promocionController;
     
    @Spy
    private List<Promocion> promociones = new ArrayList<Promocion>();
    
    @Spy
    private List<PromocionDTO> promocionesDTO = new ArrayList<PromocionDTO>();
    
    @Spy
    private List<Servicio> servicios = new ArrayList<Servicio>();
    
    @Spy
    private List<ServicioDTO> serviciosDTO = new ArrayList<ServicioDTO>();
 
    @Spy
    private ModelMap model;
     
    @Mock
    private BindingResult result;
    
    @BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		try {
			this.promociones = Promociones.promociones();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.promocionesDTO = Promociones.promocionesDTO();
		this.servicios = Servicios.getListServices();
		this.serviciosDTO = Servicios.getListServicesDTO();
	}
    
    @Test
    public void setValorFinalCeroTest() {
    	BigDecimal cero = BigDecimal.ZERO;
    	BigDecimal descuento = BigDecimal.TEN;
    	Assert.assertEquals(promocionController.setValorFinal(descuento, null), cero);
    	Assert.assertEquals(promocionController.setValorFinal(null, new ArrayList<>()), cero);
    	Assert.assertEquals(promocionController.setValorFinal(descuento, new ArrayList<>()), cero);
    }
    
    @Test
    public void setValorFinalTest() {
    	BigDecimal valor = new BigDecimal("405");
    	BigDecimal descuento = BigDecimal.TEN;
    	Assert.assertEquals(promocionController.setValorFinal(descuento, this.servicios), valor);
    }
    
    @Test
    public void getServiciosActivosTest(){
    	when(servicioService.findAllServicios()).thenReturn(this.servicios);
    	Assert.assertEquals(promocionController.getServiciosActivos().size(), 2);
    }
    
    @Test
    public void obtenerIdsTest(){
    	Assert.assertEquals(promocionController.obtenerIds(this.servicios).size(), 3);
    }
    
    @Test
    public void obtenerServiciosTest(){
    	when(servicioService.findServicioById(anyInt())).thenReturn(this.servicios.get(0));
    	when(servicioService.findServicioById(anyInt())).thenReturn(this.servicios.get(1));
    	when(servicioService.findServicioById(anyInt())).thenReturn(this.servicios.get(2));
    	List<Integer> ids = new ArrayList<>();
    	ids.add(1);
    	ids.add(2);
    	ids.add(3);
    	Assert.assertEquals(promocionController.obteberServicios(ids).size(), 3);
    }
    
    @Test //Pendiente
    public void finAllPromocionesDTOTest() {
    	when(promocionService.findAllPromociones()).thenReturn(this.promociones);
    	Assert.assertEquals(promocionController.finAllPromocionesDTO().size(), 1);
    }
    
    @Test
    public void detalleTest() {
    	when(promocionService.findPromocionById(anyInt())).thenReturn(this.promociones.get(0));
    	Assert.assertEquals(promocionController.detalle(anyInt(),model), "promociones/detallePromocion");
    }
    
    @Test
    public void activeTest() {
    	Promocion p = this.promociones.get(1);
    	when(promocionService.findPromocionById(anyInt())).thenReturn(p);
    	doNothing().when(promocionService).updatePromocion(any(Promocion.class));
    	Assert.assertEquals(promocionController.active(p.getId(), model), "redirect:/promociones/list");
    	Assert.assertEquals(model.get("mensaje"), "La promomocion "+p.getNombre()+" fue dada de Alta");
    	Assert.assertEquals(p.getEstado(), EstadoPromocion.ACTIVO);
    	Assert.assertNull(p.getFechaDeBaja());
    }
    
    @Test
    public void deleteTest() {
    	Promocion p = this.promociones.get(0);
    	when(promocionService.findPromocionById(anyInt())).thenReturn(p);
    	doNothing().when(promocionService).updatePromocion(any(Promocion.class));
    	Assert.assertEquals(promocionController.delete(p.getId(), model), "redirect:/promociones/list");
    	Assert.assertEquals(model.get("mensaje"), "La promomocion "+p.getNombre()+" fue dada de Baja");
    	Assert.assertEquals(p.getEstado(), EstadoPromocion.INACTIVO);
    	Assert.assertNotNull(p.getFechaDeBaja());
    }
    
    @Test
    public void listPromocionesTest(){
        when(promocionService.findAllPromociones()).thenReturn(this.promociones);
        Assert.assertEquals(promocionController.list(model), "promociones/listPromociones");
//        Assert.assertEquals(model.get("promocionesDTO"), this.promocionesDTO);
        verify(promocionService, atLeastOnce()).findAllPromociones();
    }
    
    @Test
    public void createTest(){
        Assert.assertEquals(promocionController.create(model), "promociones/formPromocion");
        Assert.assertNotNull(model.get("promocionDTO"));
        Assert.assertNotNull(model.get("servicios"));
        Assert.assertFalse((Boolean)model.get("edit"));
    }
    
    @Test
    public void updatePromocionErrorTest() {
    	when(result.hasErrors()).thenReturn(true);
    	doNothing().when(promocionService).updatePromocion(any(Promocion.class));
    	Assert.assertEquals(promocionController.updatePromocion(promocionesDTO.get(0), result, model), "promociones/formPromocion");
    }
    
    @Test
    public void saveErrorTest() {
    	when(result.hasErrors()).thenReturn(true);
        doNothing().when(promocionService).savePromocion(any(Promocion.class));
        Assert.assertEquals(promocionController.save(this.promocionesDTO.get(0), result, model), "promociones/formPromocion");
    }
    
    @Test
    public void saveTest() {
    	when(result.hasErrors()).thenReturn(false);
        doNothing().when(promocionService).savePromocion(any(Promocion.class));
        Promocion p = promociones.get(0);
        when(promocionService.convertToPromocion(this.promocionesDTO.get(0))).thenReturn(p);
        Assert.assertEquals(promocionController.save(promocionesDTO.get(0), result, model), "redirect:/promociones/list");
        Assert.assertEquals(model.get("mensaje"), "La Promoción "+ p.getNombre() + " fue guardada con Exito");
    }
    
    @Test
    public void saveInactivoTest() {
    	when(result.hasErrors()).thenReturn(false);
        doNothing().when(promocionService).savePromocion(any(Promocion.class));
        Promocion p = promociones.get(1);
        when(promocionService.convertToPromocion(this.promocionesDTO.get(1))).thenReturn(p);
        Assert.assertEquals(promocionController.save(promocionesDTO.get(1), result, model), "redirect:/promociones/list");
        Assert.assertEquals(model.get("mensaje"), "La Promoción "+ p.getNombre() + " fue guardada con Exito");
    }
    
    @Test
    public void updatePromocionTest() {
    	when(result.hasErrors()).thenReturn(false);
        doNothing().when(promocionService).updatePromocion(any(Promocion.class));
        Promocion p = promociones.get(0);
        when(promocionService.convertToPromocion(this.promocionesDTO.get(0))).thenReturn(p);
        Assert.assertEquals(promocionController.updatePromocion(promocionesDTO.get(0), result, model), "redirect:/promociones/list");
        Assert.assertEquals(model.get("mensaje"), "La Promoción "+ p.getNombre() + " fue actualizada con Exito");
    }
    
    @Test
    public void updatePromocionInactivaTest() {
    	when(result.hasErrors()).thenReturn(false);
        doNothing().when(promocionService).updatePromocion(any(Promocion.class));
        Promocion p = promociones.get(1);
        when(promocionService.convertToPromocion(this.promocionesDTO.get(1))).thenReturn(p);
        Assert.assertEquals(promocionController.updatePromocion(promocionesDTO.get(1), result, model), "redirect:/promociones/list");
        Assert.assertEquals(model.get("mensaje"), "La Promoción "+ p.getNombre() + " fue actualizada con Exito");
    }
    
    @Test
    public void editTest() {
    	Promocion p = promociones.get(0);
    	p.setServicios(this.servicios);
        when(this.promocionService.findPromocionById(anyInt())).thenReturn(p);
        when(promocionService.convertToDTO(p)).thenReturn(this.promocionesDTO.get(0));
        Assert.assertEquals(promocionController.edit(anyInt(), model), "promociones/formPromocion");
        Assert.assertNotNull(model.get("promocionDTO"));
        Assert.assertTrue((Boolean)model.get("edit"));
    }
}
