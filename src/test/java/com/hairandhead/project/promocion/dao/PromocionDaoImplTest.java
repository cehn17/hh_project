package com.hairandhead.project.promocion.dao;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.servicio.model.EstadoServicio;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.utils.EntityDaoImplTest;

public class PromocionDaoImplTest extends EntityDaoImplTest {

	@Autowired
	PromocionDao promocionDao;
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Promocion.xml"));
		return dataSet;
	}
	
	@Test
	public void findByIdTest() {
		Assert.assertNotNull(promocionDao.findById(1));
	}
	
	@Test
	public void findAllTest() {
		Assert.assertEquals(promocionDao.findAll().size(), 1);
	}
	@Test
	public void saveTest() {
		Promocion p = new Promocion();
		p.setNombre("Kun Aguero");
		Servicio s1 = new Servicio();
		s1.setNombre("Corte");
		s1.setDescripcion("Corte fachero masculino");
		s1.setPrecio(new BigDecimal(370));
		s1.setTiempoPromedioDeDuracion(Duration.ofMinutes(35));
		s1.setEstado(EstadoServicio.ACTIVO);
		
		Servicio s2 = new Servicio();
		s2.setNombre("Tintura");
		s2.setDescripcion("Tintura colores modernos");
		s2.setPrecio(new BigDecimal(650));
		s2.setTiempoPromedioDeDuracion(Duration.ofMinutes(120));
		s2.setEstado(EstadoServicio.ACTIVO);
		
		List<Servicio> servicios = new ArrayList<>();
		servicios.add(s1);
		servicios.add(s2);
		
		p.setServicios(servicios);
		p.setDescuento(new BigDecimal(40));
		
		promocionDao.save(p);
		
		Assert.assertEquals(promocionDao.findAll().size(), 2);
	}
	
	@Test
	public void updateTest() {
		Promocion p = promocionDao.findById(1);
		p.setMultiplicaPuntos(new BigDecimal(3));
		
		promocionDao.update(p);
		
		Assert.assertEquals(promocionDao.findById(1).getMultiplicaPuntos(), new BigDecimal(3));
	}
	
	@Test 
	public void findByEstadoTest() {
		Promocion p = new Promocion();
		p.setNombre("Kun Aguero");
		Servicio s1 = new Servicio();
		s1.setNombre("Corte");
		s1.setDescripcion("Corte fachero masculino");
		s1.setPrecio(new BigDecimal(370));
		s1.setTiempoPromedioDeDuracion(Duration.ofMinutes(35));
		s1.setEstado(EstadoServicio.ACTIVO);
		
		promocionDao.save(p);
		Assert.assertEquals(promocionDao.findByEstado(EstadoPromocion.ACTIVO).size(), 1);
	}

}
