package com.hairandhead.project.user.dao;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.hairandhead.project.user.model.EstadoUser;
import com.hairandhead.project.user.model.User;
import com.hairandhead.project.utils.EntityDaoImplTest;

public class UserDaoImplTest extends EntityDaoImplTest {
	@Autowired
	UserDao userDao;

	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("APP_USER.xml"));
		return dataSet;
	}
	
	@Test
	public void findByIdTest() {
		Assert.assertNotNull(userDao.findById(1));
	}
	
	@Test
	public void findBySSOTest() {
		Assert.assertNotNull(userDao.findBySSO("ger"));
	}
	
	@Test
	public void findByEmailTest() {
		Assert.assertNotNull(userDao.findByEmail("gecos@gmail.com"));
	}
	
	@Test
	public void findAllUsersTest() {
		Assert.assertEquals(userDao.findAllUsers().size(), 1);
	}
	
	@Test
	public void saveTest() {
		User user = new User();
		user.setEmail("dub@gmail.com");
		user.setFirstName("Dub");
		user.setLastName("Bud");
		user.setPassword("pass");
		user.setSsoId("Dub");
		
		userDao.save(user);
		
		Assert.assertEquals(userDao.findAllUsers().size() , 2);
	}
	
	@Test
	public void deleteBySSOTest() {
		userDao.deleteBySSO("ger");
		Assert.assertEquals(userDao.findAllUsers().size(), 0);
	}
	
	
	
}
