package com.hairandhead.project.turno.utils;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.pago.utils.Pagos;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.turno.dto.EventoDTO;
import com.hairandhead.project.turno.dto.TurnoFrontDTO;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.Turno;

public class Turnos {
	public static List<EventoDTO> getListEventDTO()
 	{
		List<EventoDTO> lista = new ArrayList<EventoDTO>();
		EventoDTO evento = new EventoDTO();
		
		evento.setTitle("Peluquería - Yuli");
		evento.setStart("2019-01-01 18:00:00");
		evento.setEnd("2019-01-01 19:00:00");
		
		EventoDTO evento2 = new EventoDTO();
		
		evento2.setTitle("Peluquería - Gustavo");
		evento2.setStart("2019-01-01 19:00:00");
		evento2.setEnd("2019-01-01 20:00:00");
		
		EventoDTO evento3 = new EventoDTO();
		
		evento3.setTitle("Tintura - Gustavo");
		evento3.setStart("2019-01-01 20:00:00");
		evento3.setEnd("2019-01-01 22:00:00");
		
		return lista;
 	}
	
	public static List<TurnoFrontDTO> getListTurnosFrontDTO()
 	{
		List<TurnoFrontDTO> turnoList = new ArrayList<TurnoFrontDTO>();
		TurnoFrontDTO turno = new TurnoFrontDTO();
		turno.setTurnoId("1");
		turno.setEmail("yuliana.vargas29@gmail.com");
		turno.setName("Yuli");
		turno.setSurname("Vargas");
		turno.setTotalPrice("20");
		turno.setStatus("RESERVADO");
		
		TurnoFrontDTO turno2 = new TurnoFrontDTO();
		turno.setTurnoId("2");
		turno2.setEmail("gustav@gmail.com");
		turno2.setName("Gustavo");
		turno2.setSurname("Sanchez");
		turno2.setTotalPrice("60");
		turno.setStatus("RESERVADO");
		
		turnoList.add(turno);
		turnoList.add(turno);
		return turnoList;
 	}
	
	public static List<Turno> getTurnos()
	{
		List<Turno> lista = new ArrayList<>();
		
		Turno t1 = new Turno();
		t1.setId(1);
		t1.setAdministrativo("Lucas");
		try{
			t1.setFechaAlta(LocalDateTime.parse("18-11-2019T10:00:00"));
			t1.setFecha(LocalDate.now());
			t1.setHoraInicio(LocalTime.of(0,0));
			t1.setHoraFin(LocalTime.of(0,20));
		}
		catch(DateTimeParseException e){
		    e.getMessage();
		}
		ArrayList<DetalleTurno> detalleTurnos1 = new ArrayList<>();
		DetalleTurno detalleTurno1 = new DetalleTurno();
		detalleTurno1.setId(1);
		detalleTurno1.setHoraInicio(LocalTime.of(0,0));
		detalleTurno1.setHoraFin(LocalTime.of(0,15));
		Profesional profesional = new Profesional();
		detalleTurno1.setProfesional(profesional);

		Servicio servicio = new Servicio();
		servicio.setId(1);
		servicio.setPrecio(new BigDecimal(123));
		detalleTurno1.setServicio(servicio);

		detalleTurnos1.add(detalleTurno1);
		t1.setDetalleTurnos(detalleTurnos1);

		ServicioPromocionAplicada servicioAplicado = new ServicioPromocionAplicada();
		servicioAplicado.setId(1);
		servicioAplicado.setPrecio(new BigDecimal(123));

		PromocionAplicada promo = new PromocionAplicada();
		promo.setId(1);
		List<ServicioPromocionAplicada> serviciosAplicados = new ArrayList<>();
		serviciosAplicados.add(servicioAplicado);
		promo.setServicios(serviciosAplicados);
		List<PromocionAplicada> promocionList = new ArrayList<>();
		promocionList.add(promo);

		t1.setPromociones(promocionList);

		t1.setCliente(new Cliente());
		t1.setPago(Pagos.getListPagos().get(0));

		Turno t2 = new Turno();
		t2.setId(2);
		t2.setAdministrativo("Pepe");
		try{
			t2.setFechaAlta(LocalDateTime.parse("23-08-2019T10:00:00"));
			t2.setFecha(LocalDate.parse("23-08-2019"));
			t2.setHoraInicio(LocalTime.parse("10:00:00"));
			t2.setHoraFin(LocalTime.parse("12:00:00"));
		}
		catch(DateTimeParseException e){
		    e.getMessage();
		}
		lista.add(t1);
		lista.add(t2);
		
		return lista;
	}

}
