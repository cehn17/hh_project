package com.hairandhead.project.config.service;

import com.hairandhead.project.config.dao.DataConfigDao;
import com.hairandhead.project.config.model.DataConfig;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class DataConfigServiceImplTest {

    @Mock
    DataConfigDao dataConfigDao;

    @InjectMocks
    DataConfigServiceImpl dataConfigService;

    @BeforeMethod
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAllTest(){
        List<DataConfig> dataConfigList = new ArrayList<>();
        when(dataConfigDao.findAll()).thenReturn(dataConfigList);
        Assert.assertEquals(dataConfigService.findAll(), dataConfigList);
    }

    @Test
    public void findByIdTest(){
        DataConfig dataConfig = new DataConfig();
        when(dataConfigDao.findById(anyInt())).thenReturn(dataConfig);
        Assert.assertEquals(dataConfigService.findById(anyInt()), dataConfig);
    }

    @Test
    public void findByPropertyTest(){
        DataConfig dataConfig = new DataConfig();
        when(dataConfigDao.findByProperty(anyString())).thenReturn(dataConfig);
        Assert.assertEquals(dataConfigService.findByProperty(anyString()), dataConfig);
    }

    @Test
    public void saveTest(){
        DataConfig dataConfig = new DataConfig();
        doNothing().when(dataConfigDao).save(dataConfig);
        dataConfigService.save(dataConfig);
    }

    @Test
    public void updateTest(){
        DataConfig dataConfig = new DataConfig();
        doNothing().when(dataConfigDao).update(dataConfig);
        dataConfigService.update(dataConfig);
    }

    @Test
    public void deleteTest(){
        DataConfig dataConfig = new DataConfig();
        doNothing().when(dataConfigDao).delete(dataConfig);
        dataConfigService.delete(dataConfig);
    }
}
