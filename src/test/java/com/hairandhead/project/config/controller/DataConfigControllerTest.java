package com.hairandhead.project.config.controller;

import com.hairandhead.project.config.controller.DataConfigController;
import com.hairandhead.project.config.model.DataConfig;
import com.hairandhead.project.config.service.DataConfigService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ModelMap;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class DataConfigControllerTest {

    @Mock
    DataConfigService dataConfigService;

    @InjectMocks
    DataConfigController dataConfigController;

    @Spy
    ModelMap model;

    @BeforeMethod
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void listTest(){
        List<DataConfig> dataConfigList = new ArrayList<>();
        when(dataConfigService.findAll()).thenReturn(dataConfigList);
        Assert.assertEquals(dataConfigController.list(model), "dataconfig/configlist");
    }
    @Test
    public void editFormTest(){
        DataConfig dataConfig = new DataConfig();
        when(dataConfigService.findById(anyInt())).thenReturn(dataConfig);
        Assert.assertEquals(dataConfigController.editForm(anyInt(), model), "dataconfig/configform");
    }
    @Test
    public void editTest(){
        DataConfig dataConfig = new DataConfig();
        Assert.assertEquals(dataConfigController.edit(dataConfig), "redirect:/config/list");
    }
}
