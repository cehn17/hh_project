package com.hairandhead.project.pago.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JasperPrint;
import org.hibernate.annotations.Any;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.service.ClienteServiceImpl;
import com.hairandhead.project.cliente.utils.Clientes;
import com.hairandhead.project.pago.dto.DetallePagoDTO;
import com.hairandhead.project.pago.dto.PagoDTO;
import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.pago.service.DetallePagoServiceImpl;
import com.hairandhead.project.pago.service.PagoServiceImpl;
import com.hairandhead.project.pago.utils.DetallePagos;
import com.hairandhead.project.pago.utils.Pagos;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.TurnoServiceImpl;
import com.hairandhead.project.turno.utils.Turnos;

import javax.servlet.http.HttpServletResponse;

public class PagoControllerTest {
	
	@Mock
	PagoServiceImpl pagoService;
	
	@Mock
	DetallePagoServiceImpl detallePagoService;
	
	@Mock
	TurnoServiceImpl turnoService;
	
	@Mock
	ClienteServiceImpl clienteService;
	
	@InjectMocks
    PagoController pagoController;
	
	@Spy
	List<Pago> pagos = new ArrayList<Pago>();
	
	@Spy
	List<PagoDTO> pagosDTO = new ArrayList<PagoDTO>();
	
	@Spy
	List<Turno> turnos = new ArrayList<Turno>();

	
	@Spy
	List<DetallePago> detallePagos = new ArrayList<DetallePago>();
	
	@Spy
	List<DetallePagoDTO> detallePagosDTO = new ArrayList<DetallePagoDTO>();
	
	@Spy
	List<Cliente> clientes = new ArrayList<Cliente>();
	
	@Mock
	BindingResult result;
	
	@Spy
    ModelMap model;
	
	@Mock
	RedirectAttributes attributes;

	@Mock
	HttpServletResponse response;
	
	@BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		pagos = Pagos.getListPagos();
		pagosDTO = Pagos.getListPagosDTO();
		this.turnos = Turnos.getTurnos();
		this.detallePagos =DetallePagos.getDetallePagos();
		this.detallePagosDTO = DetallePagos.getDetallePagosDTO();
		this.clientes = Clientes.getClientesList();
	}
	
	@Test
	public void listPagosTest(){
		when(pagoService.obtenerPagos()).thenReturn(pagos);
		Assert.assertEquals(pagoController.listarPagos(model), "/pagos/listarPagos");
		Assert.assertEquals(model.get("serviceDTO"), null);
		verify(pagoService, atLeastOnce()).obtenerPagos();
	}

	@Test
	public void createTest(){
		when(turnoService.findById(anyInt())).thenReturn(turnos.get(0));
		when(pagoService.buscarPorId(anyInt())).thenReturn(new Pago());
		when(detallePagoService.convertDetallePagoToDTOList(new Pago())).thenReturn(new ArrayList<>());
		Assert.assertEquals(pagoController.create(anyInt(), model), "/pagos/formPago");
	}


//	@Test -->> NO DESCOMENTAR SIN ARREGLAR, SE TILDA
//    public void createPagoTest(){
//		Turno t = this.turnos.get(0);
//        when(turnoService.findById(anyInt())).thenReturn(t);
//        PagoDTO pdto = this.pagosDTO.get(0);
//        Pago p = this.pagos.get(0);
//        when(pagoService.buscarPorId(anyInt())).thenReturn(p);
//
//        when(detallePagoService.convertDetallePagoToDTOList(any(Pago.class))).thenReturn(pdto.getDetallesDePago());
//		
//        Assert.assertEquals(pagoController.create(t.getPago().getId(),model), "/pagos/formPago");
//        Assert.assertNotNull(model.get("pagoDTO"));
//        Assert.assertFalse((Boolean)model.get("edit"));
//    }
	
	@Test
	public void createReservaDTO() {
		doNothing().when(detallePagoService).convertToDetallePago(any(DetallePago.class),any(DetallePagoDTO.class));
		Pago p = this.pagos.get(0);
		DetallePago dp = this.detallePagos.get(0);
		Turno t = this.turnos.get(0);
		when(pagoService.buscarPorId(anyInt())).thenReturn(p);
		when(turnoService.findById(anyInt())).thenReturn(t);
		doNothing().when(pagoService).editar(p);
		doNothing().when(clienteService).updateCliente(any(Cliente.class));
		
		Assert.assertEquals(pagoController.createReserva(this.detallePagosDTO.get(0),model,result), 
				"redirect:/pagos/create?idTurno="+this.detallePagosDTO.get(0).getIdTurno());
	}
	
	/* @Test /// REVISAR ESTE Y PREGUNTAR
	 public void savePagoExitosoTest(){
//	        when(result.hasErrors()).thenReturn(false);
		 when(pagoService.convertToPago(any(PagoDTO.class), any(Pago.class))).thenReturn(true);
		 doNothing().when(pagoService).guardar(any(Pago.class));
		 Assert.assertEquals(pagoController.save(pagosDTO.get(0), model, attributes), "redirect:/pagos/list");
	}
	 
	 @Test /// REVISAR ESTE Y PREGUNTAR
	 public void savePagoFracasadoTest(){
//	        when(result.hasErrors()).thenReturn(false);
		 when(pagoService.convertToPago(any(PagoDTO.class), any(Pago.class))).thenReturn(false);
		 doNothing().when(pagoService).guardar(any(Pago.class));
		 Assert.assertEquals(pagoController.save(pagosDTO.get(0), model, attributes), "redirect:/pagos/create");
	}
	*//*
	 @Test
	 public void editPagoTest(){
		 Assert.assertEquals(pagoController.edit((anyInt()), model), "pagos/formPago");
//	        Assert.assertNotNull(model.get("servicio"));
		 Assert.assertTrue((Boolean)model.get("edit"));
	 }
	 
	 @Test
	 public void updatePagoTest(){
		 doNothing().when(pagoService).editar(any(Pago.class));
		 Assert.assertEquals(pagoController.updatePago(pagosDTO.get(0), model), "redirect:/pagos/list");
	 }*/
	
}
