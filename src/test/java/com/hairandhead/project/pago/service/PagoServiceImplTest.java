package com.hairandhead.project.pago.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.TurnoService;
import net.sf.jasperreports.engine.JasperPrint;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.pago.dao.PagoDao;
import com.hairandhead.project.pago.dto.DetallePagoDTO;
import com.hairandhead.project.pago.dto.PagoDTO;
import com.hairandhead.project.pago.model.EstadoPago;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.pago.model.TipoDetallePago;
import com.hairandhead.project.pago.utils.Pagos;

public class PagoServiceImplTest {

	@Mock
	TurnoService turnoService;

	@Mock
	ServicioService servicioService;

	@Mock
	@Autowired
	PagoDao dao;
	
	@InjectMocks
	PagoServiceImpl pagoService;
	
	@Spy
	List<Pago> pagos = new ArrayList<Pago>();
	
	@Spy
	List<PagoDTO> pagosDTO = new ArrayList<PagoDTO>();
	
	@BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		pagos = Pagos.getListPagos();
		pagosDTO = Pagos.getListPagosDTO();
	}
	
	@Test
	public void findByIdTest(){
		Pago p = pagos.get(0);
		when(dao.buscarPorId(anyInt())).thenReturn(p);
		Assert.assertEquals(pagoService.buscarPorId(p.getId()),p);
		//Assert.assertEquals(pagoService.buscarPorId(p.getId()).getMonto(),p.getMonto());
		Assert.assertEquals(pagoService.buscarPorId(p.getId()).getEstado(),p.getEstado());
		Assert.assertEquals(pagoService.buscarPorId(p.getId()).getFecha(),p.getFecha());
	}
	
	@Test
	public void getEquals()
	{
		Assert.assertTrue(this.pagos.get(0).equals(this.pagos.get(0)));
		Assert.assertFalse(this.pagos.get(0).equals(this.pagos.get(1)));
	}
	
	@Test
	public void savePagoTest()
	{
		doNothing().when(dao).guardar(any(Pago.class));
		pagoService.guardar(any(Pago.class));
		verify(dao, atLeastOnce()).guardar(any(Pago.class));
	}
	
	@Test
    public void updatePagoTest()
    {
		Pago p = this.pagos.get(0);
		when(dao.buscarPorId(anyInt())).thenReturn(p);
		this.pagoService.editar(p);
		verify(dao, atLeastOnce()).editar(any(Pago.class));
    }
	
	@Test
	public void convertToPagoTest()
	{
		Pago p = new Pago();
		
		Assert.assertTrue(this.pagoService.convertToPago(pagosDTO.get(2),p));
		pagosDTO.get(2).setEstado("PAGADO");
		Assert.assertTrue(this.pagoService.convertToPago(pagosDTO.get(2),p));
//		Assert.assertEquals(this.pagos.get(0),p);
	}
	
	@Test
	public void convertToDTOPagadoTest()
	{
		PagoDTO pagoDTO = new PagoDTO();
		this.pagoService.convertToDTO(this.pagos.get(0), pagoDTO);
		Assert.assertNotEquals(pagoDTO, this.pagosDTO.get(0));
		
		pagos.get(0).setFecha(LocalDate.parse("2019-10-01"));
		pagoDTO.setFecha("2019-10-01");
		Assert.assertNotEquals(pagoDTO, this.pagosDTO.get(0));
	}

	@Test
	public void findAllPagosTest(){
		 when(dao.obtenerPagos()).thenReturn(this.pagos);
		 Assert.assertEquals(this.pagoService.obtenerPagos(), pagos);
	}
	
	@Test
	public void hashCodeTest() {
		Pago p = new Pago();
		p.setDetallePago(new ArrayList<>());
		p.setEstado(EstadoPago.PAGADO);
		p.setFecha(LocalDate.now());
		p.setId(1);
		p.setMontoAbonado(new BigDecimal(17));
		p.setMontoTotal(new BigDecimal(17));
		Assert.assertNotEquals(p, pagos.get(0));
	}
	
	@Test
	public void hashCodeNullTest() {
		Pago p = new Pago();
		p.setDetallePago(null);
		p.setEstado(null);
		p.setFecha(null);
		p.setId(null);
		p.setMontoAbonado(null);
		p.setMontoTotal(null);
		Assert.assertNotEquals(p, pagos.get(0));
	}

	@Test(expectedExceptions = {Exception.class})
	public void convertToPagoReporteTest(){
		Turno turno = new Turno();
		List<PromocionAplicada> promocionAplicadaList = new ArrayList<>();
		PromocionAplicada promocionAplicada = new PromocionAplicada();
		promocionAplicada.setDescuento(new BigDecimal("12"));
		promocionAplicadaList.add(promocionAplicada);
		turno.setPromociones(promocionAplicadaList);
		turno.setId(1);
		Pago pago = new Pago();
		pago.setId(1);
		DetallePago detallePago = new DetallePago();
		List<DetallePago> detallePagoList = new ArrayList<>();
		detallePagoList.add(detallePago);
		pago.setDetallePago(detallePagoList);
		when(pagoService.buscarPorId(anyInt())).thenReturn(pago);
		when(turnoService.findById(anyInt())).thenReturn(turno);
		when(servicioService.convertToDTO(any(Servicio.class))).thenReturn(new ServicioDTO());
		when(pagoService.exportPagoPdf(1,1)).thenReturn(new JasperPrint());
	}

	@Test
	public void convertToPago(){
		Pago pago = new Pago();
		PagoDTO pagoDTO = new PagoDTO();
		pagoService.convertToPago(pagoDTO, pago);
	}

	@Test
	public void convertToDTO(){
		Pago pago = new Pago();
		PagoDTO pagoDTO = new PagoDTO();
		pagoService.convertToDTO(pago, pagoDTO);
	}
	
	@Test
	public void pagoDTOTest() {
		PagoDTO p = this.pagosDTO.get(0);
		p.setIdTurno(10);
		p.setPuntosAcumulados(10);
		p.setDetallesDePago(new ArrayList<>());
		Assert.assertEquals(p.getMontoTotal(), p.getMontoTotal());
		Assert.assertEquals(p.getFecha(), p.getFecha());
		Assert.assertEquals(p.getIdTurno(), p.getIdTurno());
		Assert.assertEquals(p.getPuntosAcumulados(), p.getPuntosAcumulados());
		Assert.assertEquals(p.getDetallesDePago(), p.getDetallesDePago());
	}
	
	
	@Test
	public void detallePagoHashCodeTest() {
		DetallePago dp = new DetallePago();
		dp.setId(null);
		dp.setFecha(null);
		dp.setMonto(null);
		dp.setTipo(null);
		DetallePago dp2 = new DetallePago();
		dp2.setId(1);
		dp2.setFecha(LocalDateTime.now());
		dp2.setMonto(new BigDecimal(17));
		dp2.setTipo(TipoDetallePago.EFECTIVO);
		Assert.assertNotEquals(dp.hashCode(), dp2.hashCode());
		Assert.assertNotEquals(dp.getId(), dp2.getId());
		Assert.assertNotEquals(dp.toString(), dp2.toString());
	}
	
	@Test 
	public void detallePagoEqualTest() {
		DetallePago dp1 = new DetallePago();
		DetallePago dp2 = null;
		Assert.assertFalse(dp1.equals(dp2));
		
		dp2 = new DetallePago();
		dp1.setFecha(null);
		dp2.setFecha(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setFecha(null);
		dp2.setFecha(LocalDateTime.now());
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setFecha(LocalDateTime.now());
		dp2.setFecha(LocalDateTime.now().plusDays(10));
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setFecha(LocalDateTime.now());
		dp2.setFecha(dp1.getFecha());
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setId(null);
		dp2.setId(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setId(null);
		dp2.setId(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setId(1);
		dp2.setId(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setId(3);
		dp2.setId(3);
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setMonto(null);
		dp2.setMonto(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setMonto(null);
		dp2.setMonto(new BigDecimal(17));
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setMonto(new BigDecimal(13));
		dp2.setMonto(new BigDecimal(17));
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setMonto(new BigDecimal(17));
		dp2.setMonto(new BigDecimal(17));
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setTipo(null);
		dp2.setTipo(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setTipo(null);
		dp2.setTipo(TipoDetallePago.EFECTIVO);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setTipo(TipoDetallePago.PUNTOS);
		dp2.setTipo(TipoDetallePago.EFECTIVO);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setTipo(TipoDetallePago.EFECTIVO);
		dp2.setTipo(TipoDetallePago.EFECTIVO);
		Assert.assertTrue(dp1.equals(dp2));
	}
	
	@Test
	public void detallePagoDTOHashCodeTest() {
		DetallePagoDTO dp = new DetallePagoDTO();
		dp.setId(null);
		dp.setFecha(null);
		dp.setMonto(null);
		dp.setTipo(null);
		dp.setIdPago(null);
		dp.setIdTurno(null);
		DetallePagoDTO dp2 = new DetallePagoDTO();
		dp2.setId(1);
		dp2.setFecha("23/08/1988");
		dp2.setMonto("17");
		dp2.setTipo("PATACON");
		dp2.setIdTurno(17);
		dp2.setIdPago(32);
		Assert.assertNotEquals(dp.hashCode(), dp2.hashCode());
		Assert.assertNotEquals(dp.getId(), dp2.getId());
		Assert.assertNotEquals(dp.getFecha(), dp2.getFecha());
		Assert.assertNotEquals(dp.getMonto(), dp2.getMonto());
		Assert.assertNotEquals(dp.getTipo(), dp2.getTipo());
		Assert.assertNotEquals(dp.getIdPago(), dp2.getIdPago());
		Assert.assertNotEquals(dp.getIdTurno(), dp2.getIdTurno());
		Assert.assertNotEquals(dp.toString(), dp2.toString());
	}
	
	@Test 
	public void detallePagoDTOEqualTest() {
		DetallePagoDTO dp1 = new DetallePagoDTO();
		DetallePagoDTO dp2 = null;
		Assert.assertFalse(dp1.equals(dp2));
		
		dp2 = new DetallePagoDTO();
		dp1.setFecha(null);
		dp2.setFecha(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setFecha(null);
		dp2.setFecha("f2");
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setFecha("f1");
		dp2.setFecha("f2");
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setFecha("f2");
		dp2.setFecha("f2");
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setId(null);
		dp2.setId(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setId(null);
		dp2.setId(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setId(1);
		dp2.setId(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setId(3);
		dp2.setId(3);
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setMonto(null);
		dp2.setMonto(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setMonto(null);
		dp2.setMonto("17");
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setMonto("13");
		dp2.setMonto("17");
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setMonto("17");
		dp2.setMonto("17");
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setTipo(null);
		dp2.setTipo(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setTipo(null);
		dp2.setTipo("EFECTIVO");
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setTipo("PUNTOS");
		dp2.setTipo("EFECTIVO");
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setTipo("EFECTIVO");
		dp2.setTipo("EFECTIVO");
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setIdPago(null);
		dp2.setIdPago(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setIdPago(null);
		dp2.setIdPago(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setIdPago(1);
		dp2.setIdPago(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setIdPago(3);
		dp2.setIdPago(3);
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setIdTurno(null);
		dp2.setIdTurno(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setIdTurno(null);
		dp2.setIdTurno(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setIdTurno(1);
		dp2.setIdTurno(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setIdTurno(3);
		dp2.setIdTurno(3);
		Assert.assertTrue(dp1.equals(dp2));
	}
	
	@Test
	public void PagoHashCodeTest() {
		Pago p = new Pago();
		p.setId(null);
		p.setFecha(null);
		p.setMonto(null);
		p.setMontoTotal(null);
		p.setMontoAbonado(null);
		p.setEstado(null);
		p.setDetallePago(null);
		Pago p2 = new Pago();
		p2.setId(17);
		p2.setFecha(LocalDate.now());
		p2.setMonto(new BigDecimal(17));
		p2.setMontoTotal(new BigDecimal(17));
		p2.setMontoAbonado(new BigDecimal(17));
		p2.setEstado(EstadoPago.PAGADO);
		p2.setDetallePago(new ArrayList<>());
		Assert.assertNotEquals(p.hashCode(), p2.hashCode());
	}
	
	@Test 
	public void PagoEqualTest() {
		Pago dp1 = new Pago();
		Pago dp2 = null;
		Assert.assertFalse(dp1.equals(dp2));
		
		dp2 = new Pago();
		dp1.setFecha(null);
		dp2.setFecha(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setFecha(null);
		dp2.setFecha(LocalDate.now());
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setFecha(LocalDate.now());
		dp2.setFecha(LocalDate.now().plusDays(10));
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setFecha(LocalDate.now());
		dp2.setFecha(dp1.getFecha());
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setId(null);
		dp2.setId(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setId(null);
		dp2.setId(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setId(1);
		dp2.setId(2);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setId(3);
		dp2.setId(3);
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setMonto(null);
		dp2.setMonto(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setMonto(null);
		dp2.setMonto(new BigDecimal(17));
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setMonto(new BigDecimal(13));
		dp2.setMonto(new BigDecimal(17));
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setMonto(new BigDecimal(17));
		dp2.setMonto(new BigDecimal(17));
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setEstado(null);
		dp2.setEstado(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setEstado(null);
		dp2.setEstado(EstadoPago.PAGADO);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setEstado(EstadoPago.PENDIENTE);
		dp2.setEstado(EstadoPago.PAGADO);
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setEstado(EstadoPago.PAGADO);
		dp2.setEstado(EstadoPago.PAGADO);
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setDetallePago(null);
		dp2.setDetallePago(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setDetallePago(null);
		dp2.setDetallePago(new ArrayList<>());
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setDetallePago(new LinkedList<>());
		dp2.setDetallePago(new ArrayList<>());
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setDetallePago(new ArrayList<>());
		dp2.setDetallePago(new ArrayList<>());
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setMontoAbonado(null);
		dp2.setMontoAbonado(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setMontoAbonado(null);
		dp2.setMontoAbonado(new BigDecimal(17));
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setMontoAbonado(new BigDecimal(13));
		dp2.setMontoAbonado(new BigDecimal(17));
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setMontoAbonado(new BigDecimal(17));
		dp2.setMontoAbonado(new BigDecimal(17));
		Assert.assertTrue(dp1.equals(dp2));
		
		dp1.setMontoTotal(null);
		dp2.setMontoTotal(null);
		Assert.assertTrue(dp1.equals(dp2));
		dp1.setMontoTotal(null);
		dp2.setMontoTotal(new BigDecimal(17));
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setMontoTotal(new BigDecimal(13));
		dp2.setMontoTotal(new BigDecimal(17));
		Assert.assertFalse(dp1.equals(dp2));
		dp1.setMontoTotal(new BigDecimal(17));
		dp2.setMontoTotal(new BigDecimal(17));
		Assert.assertTrue(dp1.equals(dp2));
	}
}
