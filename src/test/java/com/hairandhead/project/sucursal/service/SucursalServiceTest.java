package com.hairandhead.project.sucursal.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.sucursal.dao.SucursalDao;
import com.hairandhead.project.sucursal.dto.SucursalDTO;
import com.hairandhead.project.sucursal.model.EstadoSucursal;
import com.hairandhead.project.sucursal.model.Sucursal;

public class SucursalServiceTest {
	@Mock
	@Autowired
	SucursalDao dao;
	
	@InjectMocks
	SucursalServiceImpl sucursalService;
	
	@BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void guardarTest() {
		doNothing().when(dao).guardar(any(Sucursal.class));
		sucursalService.guardar(any(Sucursal.class));
		verify(dao, atLeastOnce()).guardar(any(Sucursal.class));
	}
	
	@Test
	public void buscarPorIdTest() {
		Sucursal sucursal = new Sucursal();
		sucursal.setEstado(EstadoSucursal.ALTA);
		sucursal.setId(1);
		sucursal.setLocalidad("San Miguel");
		sucursal.setNombre("HH1");
		
		when(dao.buscarPorId(anyInt())).thenReturn(sucursal);
		
		Assert.assertEquals(sucursalService.buscarPorId(sucursal.getId()), sucursal);
		
	}
	
	@Test
	public void editarTest() {
		doNothing().when(dao).guardar(any(Sucursal.class));
        sucursalService.update(any(Sucursal.class));
        verify(dao, atLeastOnce()).update(any(Sucursal.class));
	}
	
	@Test
	public void obtenerSucursalesTest() {
		 when(dao.obtenerSucursales()).thenReturn(new ArrayList<Sucursal>());
		 Assert.assertEquals(sucursalService.obtenerSucursales(), new ArrayList<Sucursal>());
	}
	
	@Test
	public void convertToDtoTrueTest()
	{
		Sucursal sucursal = new Sucursal();
		sucursal.setEstado(EstadoSucursal.ALTA);
		sucursal.setId(1);
		sucursal.setLocalidad("San Miguel");
		sucursal.setNombre("HH1");
		Assert.assertTrue(sucursalService.convertToDto(sucursal, new SucursalDTO()));
	}
	
	@Test
	public void convertToDtoFalseTest()
	{
		Sucursal sucursal = null;
		
		Assert.assertFalse(sucursalService.convertToDto(sucursal, new SucursalDTO()));
	}
	
	@Test
	public void convertToSucursalTrueTest()
	{
		SucursalDTO sucursaldto = new SucursalDTO();
		sucursaldto.setId(1);
		sucursaldto.setNombre("Monumental");
		sucursaldto.setLocalidad("Nuñez");
		sucursaldto.setEstado("ALTA");
		
		Assert.assertTrue(this.sucursalService.convertToSucursal(sucursaldto, new Sucursal()));
	}
	
	@Test
	public void convertToSucursalFalseTest()
	{
		SucursalDTO sucursaldto = null;
		
		
		Assert.assertFalse(this.sucursalService.convertToSucursal(sucursaldto, new Sucursal()));
	}
}
