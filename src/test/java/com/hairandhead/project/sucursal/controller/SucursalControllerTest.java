package com.hairandhead.project.sucursal.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.hairandhead.project.sucursal.dto.SucursalDTO;
import com.hairandhead.project.sucursal.model.EstadoSucursal;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.sucursal.service.SucursalService;

public class SucursalControllerTest {

	@Mock
	SucursalService serviceSucursal;

	@InjectMocks
	SucursalController sucursalController;

	@Spy
	ModelMap model;

	@Mock
	RedirectAttributes result;

	@Spy
	List<Sucursal> sucursales = new ArrayList<Sucursal>();

	@BeforeClass
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		sucursales = getSucursales();
	}

	@Test
	public void listarTest() {
		when(serviceSucursal.obtenerSucursales()).thenReturn(sucursales);
		Assert.assertEquals(sucursalController.listar(model), "/sucursales/listarSucursales");
		verify(serviceSucursal, atLeastOnce()).obtenerSucursales();
	}

	@Test
	public void crearTest() {
		Assert.assertEquals(sucursalController.crear(model), "/sucursales/formSucursal");
	}

	@Test
	public void guardarExitosoTest()
	{
		when(this.serviceSucursal.convertToSucursal(any(SucursalDTO.class), any(Sucursal.class))).thenReturn(true);
        doNothing().when(this.serviceSucursal).guardar(any(Sucursal.class));
        Assert.assertEquals(this.sucursalController.guardar(new SucursalDTO(), model, result), "redirect:/sucursales/list");
	}
	
	@Test
	public void guardarFallidoTest() {
		when(this.serviceSucursal.convertToSucursal(any(SucursalDTO.class), any(Sucursal.class))).thenReturn(false);
		doNothing().when(serviceSucursal).guardar(any(Sucursal.class));
		Assert.assertEquals(sucursalController.guardar(new SucursalDTO(), model, result), "redirect:/sucursales/create");
	}

	@Test
	public void editarTest() {
		Assert.assertEquals(sucursalController.editar(this.sucursales.get(0).getId(), model), "/sucursales/formSucursal");
	}

	@Test
	public void updateExitosoTest() {
		when(this.serviceSucursal.convertToSucursal(any(SucursalDTO.class), any(Sucursal.class))).thenReturn(true);
		doNothing().when(serviceSucursal).update(any(Sucursal.class));
		Assert.assertEquals(sucursalController.update(new SucursalDTO(), 1, model),
				"redirect:/sucursales/list");
	}
	
	@Test
	public void updateMalitoTest() {
		when(this.serviceSucursal.convertToSucursal(any(SucursalDTO.class), any(Sucursal.class))).thenReturn(false);
//		doNothing().when(serviceSucursal).update(any(Sucursal.class));
		Assert.assertEquals(sucursalController.update(new SucursalDTO(), 1, model),
				"redirect:/sucursales/list");
	}
	
	@Test
	public void editarEstadoALTATest() {
		Sucursal sucursal = new Sucursal();
		sucursal.setEstado(EstadoSucursal.ALTA);
		sucursal.setId(1);
		sucursal.setLocalidad("San Miguel");
		sucursal.setNombre("HH1");
		when(serviceSucursal.buscarPorId(anyInt())).thenReturn(sucursal);
		Assert.assertEquals(sucursalController.editarEstado( 1, model),
				"redirect:/sucursales/list");
	}
	
	@Test
	public void editarEstadoBAJATest() {
		Sucursal sucursal = new Sucursal();
		sucursal.setEstado(EstadoSucursal.BAJA);
		sucursal.setId(1);
		sucursal.setLocalidad("San Miguel");
		sucursal.setNombre("HH1");
		when(serviceSucursal.buscarPorId(anyInt())).thenReturn(sucursal);
		Assert.assertEquals(sucursalController.editarEstado( 1, model),
				"redirect:/sucursales/list");
	}

	public List<Sucursal> getSucursales() {
		List<Sucursal> ret = new ArrayList<Sucursal>();
		Sucursal s1 = new Sucursal();
		s1.setEstado(EstadoSucursal.ALTA);
		s1.setLocalidad("San Miguel");
		s1.setNombre("HH1");

		Sucursal s2 = new Sucursal();
		s2.setEstado(EstadoSucursal.BAJA);
		s2.setLocalidad("San Miguel");
		s2.setNombre("HHViejo");

		ret.add(s1);
		ret.add(s2);

		return ret;
	}
}
