package com.hairandhead.project.cliente.dao;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.model.EstadoCliente;
import com.hairandhead.project.utils.EntityDaoImplTest;

public class ClienteDaoImplTest extends EntityDaoImplTest {
	
	@Autowired
	ClienteDao clienteDao;
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Cliente.xml"));
		return dataSet;
	}
	
	@Test
	public void findByIdTest() {
		Assert.assertNotNull(clienteDao.findById(1));
	}
	
	@Test
	public void findAllTest() {
		Assert.assertEquals(clienteDao.findAll().size(), 1);
	}
	
	@Test
	public void saveTest() {
		Cliente c = new Cliente();
		c.setNombre("Lucio");
		c.setApellido("Boris");
		c.setEmail("lucio@gmail.com");
		c.setEstado(EstadoCliente.ACTIVO);
		c.setPuntosAcumulados(0);
		c.setTelefono("44651437");
		
		clienteDao.save(c);
		
		Assert.assertEquals(clienteDao.findAll().size(), 2);
	}
	
	@Test
	public void editTest() {
		Cliente c = clienteDao.findById(1);
		c.setApellido("Voris");
		c.setPuntosAcumulados(1550);
		
		clienteDao.update(c);
		
		Assert.assertEquals("Voris", clienteDao.findById(1).getApellido());
		Assert.assertNotEquals(631.50, clienteDao.findById(1).getPuntosAcumulados());
		
	}

}
