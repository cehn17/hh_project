package com.hairandhead.project.profesional.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.profesional.dto.DisponibilidadDTO;
import com.hairandhead.project.profesional.dto.HorarioDTO;
import com.hairandhead.project.profesional.dto.ProfesionalDTO;
import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Horario;
import com.hairandhead.project.profesional.model.Profesional;

public class Profesionales {

	public static List<Profesional> getProfesionales()
	{
		List<Profesional> lista = new ArrayList<>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Profesional p1 = new Profesional();
		p1.setId(1);
		p1.setApellido("Lennon");
		p1.setEmail("johnlemon@gmail.com");
		p1.setEstadoProfesional(EstadoProfesional.ACTIVO);
		try{
			p1.setFechaBaja(null);//LocalDateTime.parse("2019-01-10T00:00:00"));//format.parse("2019-10-01 00:00:00"));
			p1.setFechaDeNacimiento(null);//format.parse("1999-10-01 00:00:00"));
		}
	    catch(DateTimeParseException e){
	    	e.getMessage();
	    }
		p1.setNombre("John");
//		p1.setSucursales("HH1");
		p1.setTelefono("1190906661");
		
		Profesional p2 = new Profesional();
		p2.setId(2);
		p2.setApellido("McCarney");
		p2.setEmail("paul@gmail.com");
		p2.setEstadoProfesional(EstadoProfesional.ACTIVO);
		try{
			p2.setFechaBaja(LocalDateTime.parse("2019-10-01T00:00:00"));//format.parse("2019-10-01 00:00:00"));
			p2.setFechaDeNacimiento(LocalDate.parse("1999-10-01"));//format.parse("1999-10-01 00:00:00"));
		}
	    catch(DateTimeParseException e){
	    	e.getMessage();
	    }
		p2.setNombre("Pauk");
//		p2.setSucursales("HH2");
		p2.setTelefono("11943406661");
		
		lista.add(p1);
		lista.add(p2);
		
		return lista;
	}
	
	public static List<ProfesionalDTO> getProfesionalesDTO()
	{
		List<ProfesionalDTO> lista = new ArrayList<>();
		
		ProfesionalDTO p1 = new ProfesionalDTO();
		p1.setId(1);
		p1.setApellido("Lennon");
		p1.setEmail("johnlemon@gmail.com");
		p1.setEstadoProfesional("ACTIVO");
		p1.setFechaBaja("01/10/2019 00:00:00");
		p1.setFechaDeNacimiento("01/10/1999");
		p1.setNombre("John");
//		p1.setSucursales("HH1");
		p1.setTelefono("1190906661");
		
		ProfesionalDTO p2 = new ProfesionalDTO();
		p2.setId(2);
		p2.setApellido("McCarney");
		p2.setEmail("paul@gmail.com");
		p2.setEstadoProfesional("ACTIVO");
		p2.setFechaBaja("01/10/2019");
		p2.setFechaDeNacimiento("01/10/1999");
		p2.setNombre("Pauk");
//		p2.setSucursales("HH2");
		p2.setTelefono("11943406661");
		
		lista.add(p1);
		lista.add(p2);
		
		return lista;
	}
	
	public static List<Disponibilidad> getDisponibilidades()
	{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		List<Disponibilidad> lista = new ArrayList<Disponibilidad>();
		Disponibilidad d1 = new Disponibilidad();
		d1.setId(1);
		d1.setDia("Lunes");
		
		Disponibilidad d2 = new Disponibilidad();
		d2.setId(2);
		d2.setDia("Martes");
		
		lista.add(d1);
		lista.add(d2);
		
		return lista;
	}
	
	public static List<DisponibilidadDTO> getDisponibilidadesDTO()
	{
		
		List<DisponibilidadDTO> lista = new ArrayList<DisponibilidadDTO>();
		DisponibilidadDTO d1 = new DisponibilidadDTO();
		d1.setId(1);
		d1.setDia("Lunes");
	//	d1.setHoraInicio("14:00");
		//d1.setHoraFin("20:00");
		
		DisponibilidadDTO d2 = new DisponibilidadDTO("Martes");
		d2.setId(2);
//		d2.setDia("Martes");
		//d2.setHoraInicio(null);
		//d2.setHoraFin(null);
		
		lista.add(d1);
		lista.add(d2);
		
		return lista;
	}
	
	public static List<Horario> getHorarios(){
		List<Horario> lista = new ArrayList<>();
		
		Horario h1 = new Horario();
		h1.setId(1);
		h1.setHoraInicio(LocalTime.parse("13:00:00"));
		h1.setHoraFin(LocalTime.parse("16:00:00"));
		
		Horario h2 = new Horario();
		h2.setId(2);
		h2.setHoraInicio(LocalTime.parse("08:00:00"));
		h2.setHoraFin(LocalTime.parse("12:00:00"));
		
		Horario h3 = new Horario();
		h3.setId(3);
		h3.setHoraInicio(LocalTime.parse("17:00:00"));
		h3.setHoraFin(LocalTime.parse("21:00:00"));
		
		lista.add(h1);
		lista.add(h2);
		lista.add(h2);
		return lista;
	}
	
	public static List<HorarioDTO> getHorariosDTO(){
		List<HorarioDTO> lista = new ArrayList<>();
		
		HorarioDTO h1 = new HorarioDTO();
		h1.setId(1);
		h1.setHoraInicio("13:00");
		h1.setHoraFin("16:00");
		
		HorarioDTO h2 = new HorarioDTO();
		h2.setId(2);
		h2.setHoraInicio("08:00");
		h2.setHoraFin("12:00");
		
		HorarioDTO h3 = new HorarioDTO();
		h3.setId(3);
		h3.setHoraInicio("17:00");
		h3.setHoraFin("21:00");
		
		lista.add(h1);
		lista.add(h2);
		lista.add(h2);
		return lista;
	}
}
